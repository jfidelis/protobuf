Codigo-Fonte da protobuf
https://github.com/cjh1/protobuf

Exemplo:
https://developers.google.com/protocol-buffers/docs/cpptutorial

Complilacao:

root@d5e2417e6b2b:/app/protobuf# cmake .
-- The C compiler identification is GNU 9.3.0
-- The CXX compiler identification is GNU 9.3.0
-- Check for working C compiler: /usr/bin/cc
-- Check for working C compiler: /usr/bin/cc -- works
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Detecting C compile features
-- Detecting C compile features - done
-- Check for working CXX compiler: /usr/bin/c++
-- Check for working CXX compiler: /usr/bin/c++ -- works
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Looking for pthread.h
-- Looking for pthread.h - found
-- Performing Test CMAKE_HAVE_LIBC_PTHREAD
-- Performing Test CMAKE_HAVE_LIBC_PTHREAD - Failed
-- Looking for pthread_create in pthreads
-- Looking for pthread_create in pthreads - not found
-- Looking for pthread_create in pthread
-- Looking for pthread_create in pthread - found
-- Found Threads: TRUE
-- Found ZLIB: /usr/lib/x86_64-linux-gnu/libz.so (found version "1.2.11")
-- Looking for dlfcn.h
-- Looking for dlfcn.h - found
-- Looking for inttypes.h
-- Looking for inttypes.h - found
-- Looking for memory.h
-- Looking for memory.h - found
-- Looking for stdint.h
-- Looking for stdint.h - found
-- Looking for stdlib.h
-- Looking for stdlib.h - found
-- Looking for strings.h
-- Looking for strings.h - found
-- Looking for string.h
-- Looking for string.h - found
-- Looking for sys/stat.h
-- Looking for sys/stat.h - found
-- Looking for sys/types.h
-- Looking for sys/types.h - found
-- Looking for unistd.h
-- Looking for unistd.h - found
-- Protobuf: doing try-compiles for hash map/set headers
-- Protobuf: doing try-compiles for pthread test
-- Looking for stddef.h
-- Looking for stddef.h - found
CMake Warning (dev) at src/CMakeLists.txt:326 (get_target_property):
  Policy CMP0026 is not set: Disallow use of the LOCATION target property.
  Run "cmake --help-policy CMP0026" for policy details.  Use the cmake_policy
  command to set the policy and suppress this warning.

  The LOCATION property should not be read from target "protoc_compiler".
  Use the target name directly with add_custom_command, or use the generator
  expression $<TARGET_FILE>, as appropriate.

This warning is for project developers.  Use -Wno-dev to suppress it.

-- Configuring done
-- Generating done
-- Build files have been written to: /app/protobuf
root@d5e2417e6b2b:/app/protobuf# make
Scanning dependencies of target protobuf
[  1%] Building CXX object src/CMakeFiles/protobuf.dir/google/protobuf/stubs/common.cc.o
[  2%] Building CXX object src/CMakeFiles/protobuf.dir/google/protobuf/stubs/once.cc.o
[  3%] Building CXX object src/CMakeFiles/protobuf.dir/google/protobuf/stubs/hash.cc.o
[  4%] Building CXX object src/CMakeFiles/protobuf.dir/google/protobuf/extension_set.cc.o
[  5%] Building CXX object src/CMakeFiles/protobuf.dir/google/protobuf/generated_message_util.cc.o
[  7%] Building CXX object src/CMakeFiles/protobuf.dir/google/protobuf/message_lite.cc.o
[  8%] Building CXX object src/CMakeFiles/protobuf.dir/google/protobuf/repeated_field.cc.o
[  9%] Building CXX object src/CMakeFiles/protobuf.dir/google/protobuf/wire_format_lite.cc.o
[ 10%] Building CXX object src/CMakeFiles/protobuf.dir/google/protobuf/io/coded_stream.cc.o
[ 11%] Building CXX object src/CMakeFiles/protobuf.dir/google/protobuf/io/zero_copy_stream.cc.o
[ 12%] Building CXX object src/CMakeFiles/protobuf.dir/google/protobuf/io/zero_copy_stream_impl_lite.cc.o
[ 14%] Building CXX object src/CMakeFiles/protobuf.dir/google/protobuf/stubs/strutil.cc.o
[ 15%] Building CXX object src/CMakeFiles/protobuf.dir/google/protobuf/stubs/substitute.cc.o
[ 16%] Building CXX object src/CMakeFiles/protobuf.dir/google/protobuf/stubs/structurally_valid.cc.o
[ 17%] Building CXX object src/CMakeFiles/protobuf.dir/google/protobuf/stubs/atomicops_internals_x86_gcc.cc.o
[ 18%] Building CXX object src/CMakeFiles/protobuf.dir/google/protobuf/stubs/atomicops_internals_x86_msvc.cc.o
[ 20%] Building CXX object src/CMakeFiles/protobuf.dir/google/protobuf/stubs/stringprintf.cc.o
[ 21%] Building CXX object src/CMakeFiles/protobuf.dir/google/protobuf/descriptor.cc.o
[ 22%] Building CXX object src/CMakeFiles/protobuf.dir/google/protobuf/descriptor.pb.cc.o
[ 23%] Building CXX object src/CMakeFiles/protobuf.dir/google/protobuf/descriptor_database.cc.o
[ 24%] Building CXX object src/CMakeFiles/protobuf.dir/google/protobuf/dynamic_message.cc.o
[ 25%] Building CXX object src/CMakeFiles/protobuf.dir/google/protobuf/extension_set_heavy.cc.o
[ 27%] Building CXX object src/CMakeFiles/protobuf.dir/google/protobuf/generated_message_reflection.cc.o
[ 28%] Building CXX object src/CMakeFiles/protobuf.dir/google/protobuf/message.cc.o
[ 29%] Building CXX object src/CMakeFiles/protobuf.dir/google/protobuf/reflection_ops.cc.o
[ 30%] Building CXX object src/CMakeFiles/protobuf.dir/google/protobuf/service.cc.o
[ 31%] Building CXX object src/CMakeFiles/protobuf.dir/google/protobuf/text_format.cc.o
[ 32%] Building CXX object src/CMakeFiles/protobuf.dir/google/protobuf/unknown_field_set.cc.o
[ 34%] Building CXX object src/CMakeFiles/protobuf.dir/google/protobuf/wire_format.cc.o
[ 35%] Building CXX object src/CMakeFiles/protobuf.dir/google/protobuf/io/printer.cc.o
[ 36%] Building CXX object src/CMakeFiles/protobuf.dir/google/protobuf/io/tokenizer.cc.o
[ 37%] Building CXX object src/CMakeFiles/protobuf.dir/google/protobuf/io/zero_copy_stream_impl.cc.o
[ 38%] Building CXX object src/CMakeFiles/protobuf.dir/google/protobuf/compiler/importer.cc.o
[ 40%] Building CXX object src/CMakeFiles/protobuf.dir/google/protobuf/compiler/parser.cc.o
[ 41%] Building CXX object src/CMakeFiles/protobuf.dir/google/protobuf/io/gzip_stream.cc.o
[ 42%] Linking CXX shared library ../bin/libprotobuf.so
[ 42%] Built target protobuf
Scanning dependencies of target protoc_lib
[ 43%] Building CXX object src/CMakeFiles/protoc_lib.dir/google/protobuf/compiler/code_generator.cc.o
[ 44%] Building CXX object src/CMakeFiles/protoc_lib.dir/google/protobuf/compiler/command_line_interface.cc.o
[ 45%] Building CXX object src/CMakeFiles/protoc_lib.dir/google/protobuf/compiler/cpp/cpp_enum.cc.o
[ 47%] Building CXX object src/CMakeFiles/protoc_lib.dir/google/protobuf/compiler/cpp/cpp_enum_field.cc.o
[ 48%] Building CXX object src/CMakeFiles/protoc_lib.dir/google/protobuf/compiler/cpp/cpp_extension.cc.o
[ 49%] Building CXX object src/CMakeFiles/protoc_lib.dir/google/protobuf/compiler/cpp/cpp_field.cc.o
[ 50%] Building CXX object src/CMakeFiles/protoc_lib.dir/google/protobuf/compiler/cpp/cpp_file.cc.o
[ 51%] Building CXX object src/CMakeFiles/protoc_lib.dir/google/protobuf/compiler/cpp/cpp_generator.cc.o
[ 52%] Building CXX object src/CMakeFiles/protoc_lib.dir/google/protobuf/compiler/cpp/cpp_helpers.cc.o
[ 54%] Building CXX object src/CMakeFiles/protoc_lib.dir/google/protobuf/compiler/cpp/cpp_message.cc.o
[ 55%] Building CXX object src/CMakeFiles/protoc_lib.dir/google/protobuf/compiler/cpp/cpp_message_field.cc.o
[ 56%] Building CXX object src/CMakeFiles/protoc_lib.dir/google/protobuf/compiler/cpp/cpp_primitive_field.cc.o
[ 57%] Building CXX object src/CMakeFiles/protoc_lib.dir/google/protobuf/compiler/cpp/cpp_service.cc.o
[ 58%] Building CXX object src/CMakeFiles/protoc_lib.dir/google/protobuf/compiler/cpp/cpp_string_field.cc.o
[ 60%] Building CXX object src/CMakeFiles/protoc_lib.dir/google/protobuf/compiler/plugin.cc.o
[ 61%] Building CXX object src/CMakeFiles/protoc_lib.dir/google/protobuf/compiler/plugin.pb.cc.o
[ 62%] Building CXX object src/CMakeFiles/protoc_lib.dir/google/protobuf/compiler/subprocess.cc.o
[ 63%] Building CXX object src/CMakeFiles/protoc_lib.dir/google/protobuf/compiler/zip_writer.cc.o
[ 64%] Building CXX object src/CMakeFiles/protoc_lib.dir/google/protobuf/compiler/java/java_doc_comment.cc.o
[ 65%] Building CXX object src/CMakeFiles/protoc_lib.dir/google/protobuf/compiler/java/java_enum.cc.o
[ 67%] Building CXX object src/CMakeFiles/protoc_lib.dir/google/protobuf/compiler/java/java_enum_field.cc.o
[ 68%] Building CXX object src/CMakeFiles/protoc_lib.dir/google/protobuf/compiler/java/java_extension.cc.o
[ 69%] Building CXX object src/CMakeFiles/protoc_lib.dir/google/protobuf/compiler/java/java_field.cc.o
[ 70%] Building CXX object src/CMakeFiles/protoc_lib.dir/google/protobuf/compiler/java/java_file.cc.o
[ 71%] Building CXX object src/CMakeFiles/protoc_lib.dir/google/protobuf/compiler/java/java_generator.cc.o
[ 72%] Building CXX object src/CMakeFiles/protoc_lib.dir/google/protobuf/compiler/java/java_helpers.cc.o
[ 74%] Building CXX object src/CMakeFiles/protoc_lib.dir/google/protobuf/compiler/java/java_message.cc.o
[ 75%] Building CXX object src/CMakeFiles/protoc_lib.dir/google/protobuf/compiler/java/java_message_field.cc.o
[ 76%] Building CXX object src/CMakeFiles/protoc_lib.dir/google/protobuf/compiler/java/java_primitive_field.cc.o
[ 77%] Building CXX object src/CMakeFiles/protoc_lib.dir/google/protobuf/compiler/java/java_service.cc.o
[ 78%] Building CXX object src/CMakeFiles/protoc_lib.dir/google/protobuf/compiler/java/java_string_field.cc.o
[ 80%] Building CXX object src/CMakeFiles/protoc_lib.dir/google/protobuf/compiler/python/python_generator.cc.o
[ 81%] Linking CXX shared library ../bin/libprotoc.so
[ 81%] Built target protoc_lib
Scanning dependencies of target protoc_compiler
[ 82%] Building CXX object src/CMakeFiles/protoc_compiler.dir/google/protobuf/compiler/main.cc.o
[ 83%] Linking CXX executable ../bin/protoc
[ 83%] Built target protoc_compiler
Scanning dependencies of target protobuf-lite
[ 84%] Building CXX object src/CMakeFiles/protobuf-lite.dir/google/protobuf/stubs/common.cc.o
[ 85%] Building CXX object src/CMakeFiles/protobuf-lite.dir/google/protobuf/stubs/once.cc.o
[ 87%] Building CXX object src/CMakeFiles/protobuf-lite.dir/google/protobuf/stubs/hash.cc.o
[ 88%] Building CXX object src/CMakeFiles/protobuf-lite.dir/google/protobuf/extension_set.cc.o
[ 89%] Building CXX object src/CMakeFiles/protobuf-lite.dir/google/protobuf/generated_message_util.cc.o
[ 90%] Building CXX object src/CMakeFiles/protobuf-lite.dir/google/protobuf/message_lite.cc.o
[ 91%] Building CXX object src/CMakeFiles/protobuf-lite.dir/google/protobuf/repeated_field.cc.o
[ 92%] Building CXX object src/CMakeFiles/protobuf-lite.dir/google/protobuf/wire_format_lite.cc.o
[ 94%] Building CXX object src/CMakeFiles/protobuf-lite.dir/google/protobuf/io/coded_stream.cc.o
[ 95%] Building CXX object src/CMakeFiles/protobuf-lite.dir/google/protobuf/io/zero_copy_stream.cc.o
[ 96%] Building CXX object src/CMakeFiles/protobuf-lite.dir/google/protobuf/io/zero_copy_stream_impl_lite.cc.o
[ 97%] Building CXX object src/CMakeFiles/protobuf-lite.dir/google/protobuf/stubs/atomicops_internals_x86_gcc.cc.o
[ 98%] Building CXX object src/CMakeFiles/protobuf-lite.dir/google/protobuf/stubs/atomicops_internals_x86_msvc.cc.o
[100%] Linking CXX shared library ../bin/libprotobuf-lite.so
[100%] Built target protobuf-lite
root@d5e2417e6b2b:/app/protobuf#
root@d5e2417e6b2b:/app/protobuf# make install
[ 42%] Built target protobuf
[ 81%] Built target protoc_lib
[ 83%] Built target protoc_compiler
[100%] Built target protobuf-lite
Install the project...
-- Install configuration: ""
-- Installing: /usr/local/lib/libprotobuf.so
-- Installing: /usr/local/lib/libprotobuf-lite.so
-- Installing: /usr/local/lib/libprotoc.so
-- Set runtime path of "/usr/local/lib/libprotoc.so" to ""
-- Installing: /usr/local/bin/protoc
-- Set runtime path of "/usr/local/bin/protoc" to ""
-- Installing: /usr/local/include/google
-- Installing: /usr/local/include/google/protobuf
-- Installing: /usr/local/include/google/protobuf/compiler
-- Installing: /usr/local/include/google/protobuf/compiler/code_generator.h
-- Installing: /usr/local/include/google/protobuf/compiler/command_line_interface.h
-- Installing: /usr/local/include/google/protobuf/compiler/cpp
-- Installing: /usr/local/include/google/protobuf/compiler/cpp/cpp_enum.h
-- Installing: /usr/local/include/google/protobuf/compiler/cpp/cpp_enum_field.h
-- Installing: /usr/local/include/google/protobuf/compiler/cpp/cpp_extension.h
-- Installing: /usr/local/include/google/protobuf/compiler/cpp/cpp_field.h
-- Installing: /usr/local/include/google/protobuf/compiler/cpp/cpp_file.h
-- Installing: /usr/local/include/google/protobuf/compiler/cpp/cpp_generator.h
-- Installing: /usr/local/include/google/protobuf/compiler/cpp/cpp_helpers.h
-- Installing: /usr/local/include/google/protobuf/compiler/cpp/cpp_message.h
-- Installing: /usr/local/include/google/protobuf/compiler/cpp/cpp_message_field.h
-- Installing: /usr/local/include/google/protobuf/compiler/cpp/cpp_options.h
-- Installing: /usr/local/include/google/protobuf/compiler/cpp/cpp_primitive_field.h
-- Installing: /usr/local/include/google/protobuf/compiler/cpp/cpp_service.h
-- Installing: /usr/local/include/google/protobuf/compiler/cpp/cpp_string_field.h
-- Installing: /usr/local/include/google/protobuf/compiler/cpp/cpp_unittest.h
-- Installing: /usr/local/include/google/protobuf/compiler/importer.h
-- Installing: /usr/local/include/google/protobuf/compiler/java
-- Installing: /usr/local/include/google/protobuf/compiler/java/java_doc_comment.h
-- Installing: /usr/local/include/google/protobuf/compiler/java/java_enum.h
-- Installing: /usr/local/include/google/protobuf/compiler/java/java_enum_field.h
-- Installing: /usr/local/include/google/protobuf/compiler/java/java_extension.h
-- Installing: /usr/local/include/google/protobuf/compiler/java/java_field.h
-- Installing: /usr/local/include/google/protobuf/compiler/java/java_file.h
-- Installing: /usr/local/include/google/protobuf/compiler/java/java_generator.h
-- Installing: /usr/local/include/google/protobuf/compiler/java/java_helpers.h
-- Installing: /usr/local/include/google/protobuf/compiler/java/java_message.h
-- Installing: /usr/local/include/google/protobuf/compiler/java/java_message_field.h
-- Installing: /usr/local/include/google/protobuf/compiler/java/java_primitive_field.h
-- Installing: /usr/local/include/google/protobuf/compiler/java/java_service.h
-- Installing: /usr/local/include/google/protobuf/compiler/java/java_string_field.h
-- Installing: /usr/local/include/google/protobuf/compiler/mock_code_generator.h
-- Installing: /usr/local/include/google/protobuf/compiler/package_info.h
-- Installing: /usr/local/include/google/protobuf/compiler/parser.h
-- Installing: /usr/local/include/google/protobuf/compiler/plugin.h
-- Installing: /usr/local/include/google/protobuf/compiler/plugin.pb.h
-- Installing: /usr/local/include/google/protobuf/compiler/python
-- Installing: /usr/local/include/google/protobuf/compiler/python/python_generator.h
-- Installing: /usr/local/include/google/protobuf/compiler/subprocess.h
-- Installing: /usr/local/include/google/protobuf/compiler/zip_writer.h
-- Installing: /usr/local/include/google/protobuf/descriptor.h
-- Installing: /usr/local/include/google/protobuf/descriptor.pb.h
-- Installing: /usr/local/include/google/protobuf/descriptor_database.h
-- Installing: /usr/local/include/google/protobuf/dynamic_message.h
-- Installing: /usr/local/include/google/protobuf/extension_set.h
-- Installing: /usr/local/include/google/protobuf/generated_enum_reflection.h
-- Installing: /usr/local/include/google/protobuf/generated_message_reflection.h
-- Installing: /usr/local/include/google/protobuf/generated_message_util.h
-- Installing: /usr/local/include/google/protobuf/io
-- Installing: /usr/local/include/google/protobuf/io/coded_stream.h
-- Installing: /usr/local/include/google/protobuf/io/coded_stream_inl.h
-- Installing: /usr/local/include/google/protobuf/io/gzip_stream.h
-- Installing: /usr/local/include/google/protobuf/io/package_info.h
-- Installing: /usr/local/include/google/protobuf/io/printer.h
-- Installing: /usr/local/include/google/protobuf/io/tokenizer.h
-- Installing: /usr/local/include/google/protobuf/io/zero_copy_stream.h
-- Installing: /usr/local/include/google/protobuf/io/zero_copy_stream_impl.h
-- Installing: /usr/local/include/google/protobuf/io/zero_copy_stream_impl_lite.h
-- Installing: /usr/local/include/google/protobuf/message.h
-- Installing: /usr/local/include/google/protobuf/message_lite.h
-- Installing: /usr/local/include/google/protobuf/package_info.h
-- Installing: /usr/local/include/google/protobuf/reflection_ops.h
-- Installing: /usr/local/include/google/protobuf/repeated_field.h
-- Installing: /usr/local/include/google/protobuf/service.h
-- Installing: /usr/local/include/google/protobuf/stubs
-- Installing: /usr/local/include/google/protobuf/stubs/atomicops.h
-- Installing: /usr/local/include/google/protobuf/stubs/atomicops_internals_arm_gcc.h
-- Installing: /usr/local/include/google/protobuf/stubs/atomicops_internals_arm_qnx.h
-- Installing: /usr/local/include/google/protobuf/stubs/atomicops_internals_atomicword_compat.h
-- Installing: /usr/local/include/google/protobuf/stubs/atomicops_internals_macosx.h
-- Installing: /usr/local/include/google/protobuf/stubs/atomicops_internals_mips_gcc.h
-- Installing: /usr/local/include/google/protobuf/stubs/atomicops_internals_pnacl.h
-- Installing: /usr/local/include/google/protobuf/stubs/atomicops_internals_x86_gcc.h
-- Installing: /usr/local/include/google/protobuf/stubs/atomicops_internals_x86_msvc.h
-- Installing: /usr/local/include/google/protobuf/stubs/common.h
-- Installing: /usr/local/include/google/protobuf/stubs/hash.h
-- Installing: /usr/local/include/google/protobuf/stubs/map-util.h
-- Installing: /usr/local/include/google/protobuf/stubs/once.h
-- Installing: /usr/local/include/google/protobuf/stubs/platform_macros.h
-- Installing: /usr/local/include/google/protobuf/stubs/stl_util-inl.h
-- Installing: /usr/local/include/google/protobuf/stubs/stl_util.h
-- Installing: /usr/local/include/google/protobuf/stubs/stringprintf.h
-- Installing: /usr/local/include/google/protobuf/stubs/strutil.h
-- Installing: /usr/local/include/google/protobuf/stubs/substitute.h
-- Installing: /usr/local/include/google/protobuf/stubs/template_util.h
-- Installing: /usr/local/include/google/protobuf/stubs/type_traits.h
-- Installing: /usr/local/include/google/protobuf/testing
-- Installing: /usr/local/include/google/protobuf/testing/googletest.h
-- Installing: /usr/local/include/google/protobuf/test_util.h
-- Installing: /usr/local/include/google/protobuf/test_util_lite.h
-- Installing: /usr/local/include/google/protobuf/text_format.h
-- Installing: /usr/local/include/google/protobuf/unknown_field_set.h
-- Installing: /usr/local/include/google/protobuf/wire_format.h
-- Installing: /usr/local/include/google/protobuf/wire_format_lite.h
-- Installing: /usr/local/include/google/protobuf/wire_format_lite_inl.h
root@d5e2417e6b2b:/app/protobuf#


Exemplo de uso:

c++ my_program.cc my_proto.pb.cc `pkg-config --cflags --libs protobuf`
c++  my_program.cc  -o my_program -pthread -I/usr/local/include  -pthread -L/usr/local/lib -lprotoc -lprotobuf -lpthread

Erro ao tentar gerar os arquivos protobuf
protoc: error while loading shared libraries: libprotoc.so.8: cannot open shared object file: No such file or directory

sudo ldconfig
or

export LD_LIBRARY_PATH=/usr/local/lib

Gerar os arquivos proto
protoc_middleman: addressbook.proto
	protoc -I=. --cpp_out=. addressbook.proto

addressbook.pb.cc
addressbook.pb.h

Executando demos:
add_person_cpp: add_person.cc protoc_middleman
	c++ add_person.cc addressbook.pb.cc -o add_person_cpp -pthread -I/usr/local/include  -pthread -L/usr/local/lib -lprotoc -lprotobuf -lpthread
	
list_people_cpp: list_people.cc protoc_middleman
	c++ list_people.cc addressbook.pb.cc -o list_people_cpp -pthread -I/usr/local/include  -pthread -L/usr/local/lib -lprotoc -lprotobuf -lpthread
	
LIBS          = $(SUBLIBS) /usr/lib/x86_64-linux-gnu/libQt5Gui.so /usr/lib/x86_64-linux-gnu/libQt5Core.so /usr/lib/x86_64-linux-gnu/libGL.so -lpthread -L/usr/local/lib -lprotoc -lprotobuf -lpthread
	
	
	
	
