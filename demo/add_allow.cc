#include <iostream>
#include <fstream>
#include <string>
#include "allowanaccessparams.pb.h"
using namespace std;
// c++ add_allow.cc allowanaccessparams.pb.cc -o add_allow_cpp -pthread -I/usr/local/include  -pthread -L/usr/local/lib -lprotoc -lprotobuf -lpthread

void AddAllow(tutorial::AllowAnAccessParams* allow) {
  cout << "AddAllow:";
  string code = "0";
  allow->set_code(code);

  string justification = "teste 1";
  allow->set_justification(justification);
  
  int accessdirectiontypeenum = 1;
  allow->set_accessdirectiontypeenum(accessdirectiontypeenum);  
}


int main(int argc, char* argv[]) {

  GOOGLE_PROTOBUF_VERIFY_VERSION;
  tutorial::Allow allow;
  AddAllow(allow.add_allow());
  {
    // Write the new address book back to disk.
    fstream output("allow.data", ios::out | ios::trunc | ios::binary);
    if (!allow.SerializeToOstream(&output)) {
      cerr << "Failed to write address book." << endl;
      return -1;
    }
  }

  google::protobuf::ShutdownProtobufLibrary();

  return 0;
}