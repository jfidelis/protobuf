#include <QCoreApplication>
#include <QByteArray>
#include <QBitArray>
#include <QString>
#include <QDebug>
#include <QTextStream>
#include <QDataStream>
#include "allowanaccessparams.pb.h"
#include <iostream>
using namespace std;

enum OptionOperationEnum {
  Exit = 0,
  AllowAnAccessSerialize = 1,
  AllowAnAccessDeserialize = 2
};

void Add(newaccess::AllowAnAccessParams* allow) {
	allow->set_code("10");
	allow->set_justification("Acesso Permitido");
	allow->set_accessdirectiontypeenum(newaccess::AccessDirectionType::Entrance);
}

OptionOperationEnum SelectedOperation(){
  cout << "---------------------------------------------------" << std::endl;
  cout << "|    Informe sua opcao:                           |" << std::endl;	
  cout << "---------------------------------------------------" << std::endl;
  cout << "|    1 - AllowAnAccess - Serialize                |" << std::endl;
  cout << "|    2 - AllowAnAccess - Deserialize              |" << std::endl;
  cout << "|    0 - Sair                                     |" << std::endl;
  cout << "---------------------------------------------------" << std::endl;

  int opc;
  cin >> opc;
  cin.ignore(256, '\n');

  return (OptionOperationEnum) opc;
}

void AllowAnAccessSerializeExec()
{
	newaccess::AllowAnAccessParams allowOneIn;

	cout << "Informe o Code: ";
	getline(cin, *allowOneIn.mutable_code());	

	cout << "Informe a Justificativa: ";
	getline(cin, *allowOneIn.mutable_justification());	

	cout << "Informe a Direção[1:Ent, 2:Saida, 3:Ambos, 4:Mesma]: ";
	int dir;
	cin >> dir;
	allowOneIn.set_accessdirectiontypeenum((newaccess::AccessDirectionType)dir);

	QByteArray byteArrayAllowOneIn(allowOneIn.SerializeAsString().c_str());
	qDebug() << "byteArrayAllowOneIn: " << byteArrayAllowOneIn;

	qDebug() << "Result: " << byteArrayAllowOneIn.toBase64(QByteArray::Base64Encoding); 
}

void AllowAnAccessDeserializeExec()
{
	newaccess::AllowAnAccessParams allowOneOut;
	cout << "Informe o dado(string base64 - serializado): ";

	string stringBase64Serialize;
	getline(cin, stringBase64Serialize);

	QByteArray bytesB64(stringBase64Serialize.c_str(), stringBase64Serialize.length());

	QByteArray byteArray = QByteArray::fromBase64(bytesB64);

	int error = allowOneOut.ParseFromArray(byteArray, byteArray.size());	

	if (error == 0) {
		qDebug() << "AllowAnAccessDeserializeExec Error...";
	}else{
		qDebug() << "Result: ";
		std::cout << "Code: " << allowOneOut.code() << std::endl;	
		std::cout << " Justification: " << allowOneOut.justification() << std::endl;	
		std::cout << " AccessDirectionType: " << allowOneOut.accessdirectiontypeenum() << std::endl;		
	}
}

int main(int argc, char *argv[])
{
  	GOOGLE_PROTOBUF_VERIFY_VERSION;
	
	
	newaccess::AllowAnAccessParams allowOne;
	OptionOperationEnum opcSelected;	
	
    QCoreApplication a(argc, argv);
    QTextStream stream(stdout);
    stream << "Qt Version: " << QT_VERSION_STR << endl;	

	do{
		opcSelected = SelectedOperation();

		switch (opcSelected)
		{
			case OptionOperationEnum::AllowAnAccessDeserialize :
				AllowAnAccessDeserializeExec();
				break;
			case OptionOperationEnum::AllowAnAccessSerialize:
				AllowAnAccessSerializeExec();
				continue;
				break;

			case OptionOperationEnum::Exit:
				qDebug() << "App Exit...";
				return -1;		
		default:
			break;
		}
	}while(opcSelected != OptionOperationEnum::Exit);
}
