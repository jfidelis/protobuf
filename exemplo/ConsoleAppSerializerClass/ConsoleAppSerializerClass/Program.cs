﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppSerializerClass
{
    class Program
    {
        static void ClassAllowAnAccessParamsRun()
        {
            Console.WriteLine("---------------------------------------------------------");
            Console.WriteLine("Class AllowAnAccessParams:");

            AllowAnAccessParams objIn = new AllowAnAccessParams();

            objIn.Code = "10";
            objIn.Justification = "Acesso Permitido";
            objIn.AccessDirectionTypeEnum = AccessDirectionType.Entrance;

            var protoClass = SerializationHelper.GetProto<AllowAnAccessParams>();

            Console.WriteLine("Arquivo .proto:");
            Console.WriteLine(protoClass);

            var inBytes = SerializationHelper.Serialize<AllowAnAccessParams>(objIn);

            var Base64StringIn = Convert.ToBase64String(inBytes);

            Console.WriteLine("AllowAnAccessParams Serialize and ToBase64String:");
            Console.WriteLine(Base64StringIn);
            Console.WriteLine("---------------------------------------------------------");
        }

        static void Main(string[] args)
        {
            ClassAllowAnAccessParamsRun();

            //string ResultIsBase64String = "CgEwEgd0ZXN0ZSAxGAE=";
            //var binaryData = Convert.FromBase64String(ResultIsBase64String);
            //AllowAnAccessParams obj = SerializationHelper.Deserialize<AllowAnAccessParams>(binaryData);
            Console.ReadKey();
        }
    }
}
