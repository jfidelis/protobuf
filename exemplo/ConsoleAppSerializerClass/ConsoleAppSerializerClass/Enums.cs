﻿using System.Runtime.Serialization;
using System;

namespace ConsoleAppSerializerClass
{

    #region HiddenEnum class

    /// <summary>
    /// This class indicates that the element's enum will be hidden at the web components
    /// </summary>
    public class HiddenEnum : Attribute
    {

    }

    #endregion

    /// <summary>
    /// Indicates the field type
    /// </summary>
    public enum FieldTypeEnum : int
    {
        /// <summary>
        /// Indicates that the field is numeric only.
        /// </summary>
        Numeric = 1,
        /// <summary>
        /// Indicates that the field is alphanumeric
        /// </summary>
        Alphanumeric = 2
    }

    /// <summary>
    /// Indicates which is the master type for a given equipment
    /// </summary>
    public enum MasterTypeEnum : int
    {
        /// <summary>
        /// Indicates that the type is not a master type
        /// </summary>
        NotMaster = 0,
        /// <summary>
        /// Indicates that the type is a master in all access
        /// </summary>
        IsMaster = 1,
        /// <summary>
        /// Indicates that the type is a master only in it's profile
        /// </summary>
        IsMasterInProfile = 2
    }

    /// <summary>
    ///  Indicates the database type selected or supported by the application
    /// </summary>
    public enum DatabaseTypeEnum : int
    {
        /// <summary>
        /// Indicates that the SQL Server is the database type selected or supported by the application
        /// </summary>
        SQLServer = 1,
        /// <summary>
        /// Indicates that the Oracle, using the native client installed as the data provider, is the database type selected or supported by the application
        /// </summary>
        OracleDP = 3,
        /// <summary>
        /// Indicates that the Postgre SQL is the database type selected or supported by the application
        /// </summary>
        PostgreSQL = 4
    }

    /// <summary>
    /// Indicates which is the operation type for a given equipment
    /// </summary>
    public enum EquipmentOperationTypeEnum : int
    {
        /// <summary>
        /// Indicates that the equipment is working as an access controller only
        /// </summary>
        AccessController = 0,
        /// <summary>
        /// Indicates that the equipment is working as a time register only
        /// </summary>
        TimeRegister = 1,
        /// <summary>
        /// Indicates that the equipment is working in both operation mode: as a time register and as an access controller
        /// </summary>
        AccessControllerAndTimeRegister = 2
    }

    /// <summary>
    /// Indicates a communication type used by the equipment
    /// </summary>
    public enum CommunicationTypeEnum : int
    {
        /// <summary>
        /// Indicates that the communication type used is Serial
        /// </summary>
        Serial = 1,
        /// <summary>
        /// Indicates that the communication type used is TCP IP
        /// </summary>
        TCP_IP = 2,
        /// <summary>
        /// The TCP ip server
        /// </summary>
        TCP_IP_Server = 3
    }

    /// <summary>
    /// Indicates the duration of a credential
    /// </summary>
    public enum DurationTypeEnum : int
    {
        /// <summary>
        /// Indicates that the credential is temporary
        /// </summary>
        Temporary = 0,
        /// <summary>
        /// Indicates that the credential is permanent
        /// </summary>
        Permanent = 1
    }

    /// <summary>
    /// Indicates which entity the credential belongs to
    /// </summary>
    public enum CredentialAuthenticationTypeEnum : int
    {
        /// <summary>
        /// Indicates that the credential belongs to the Person entity
        /// </summary>
        Person = 1,
        /// <summary>
        /// Indicates that the credential belongs to the Visitor entity
        /// </summary>
        Visitor = 2,
        /// <summary>
        /// Indicates that the credential belongs to the Credential entity
        /// </summary>
        Credential = 3
    }

    /// <summary>
    /// The access status of a credential
    /// </summary>
    public enum CredencialStatusTypeEnum : int
    {
        /// <summary>
        /// Indicates that credential status value is 'Valid'
        /// </summary>
        Valid = 0,
        /// <summary>
        /// Indicates that credential status value is 'Blocked'
        /// </summary>
        Blocked = 1,
        /// <summary>
        /// ONLY FOR COMMUNICATION PURPOSE.
        /// Indicates that credential status value is 'Both'
        /// </summary>
        [SwaggerHiddenEnum]
        Both = 2
    }

    /// <summary>
    /// Indicates the Flag of validity
    /// </summary>
    public enum ValidityEnum : int
    {
        /// <summary>
        /// Indicates that validity is in days
        /// </summary>
        Days = 0,
        /// <summary>
        /// Indicates that validity is in hours
        /// </summary>
        Hours = 1
    }

    /// <summary>
    /// Determines the entrance direction
    /// </summary>
    public enum EntranceDirectionTypeEnum : int
    {
        /// <summary>
        /// Indicates that entrance direction is 'left'
        /// </summary>
        Left = 0,
        /// <summary>
        /// Indicates that entrance direction is 'right'
        /// </summary>
        Right = 1
    }

    /// <summary>
    /// Indicates how many digits are used to encryption
    /// </summary>
    public enum EncryptionTypeEnum : int
    {
        /// <summary>
        /// Indicates that none encryption type will be used
        /// </summary>
        None = 0,
        /// <summary>
        /// Indicates that will be used eigh digits for encryption
        /// </summary>
        EightDigits = 1,
        /// <summary>
        /// Indicates that will be used twelve digits for encryption
        /// </summary>
        TwelveDigits = 2,
        /// <summary>
        /// Indicates that will be used ten digits for encryption
        /// </summary>
        TenDigits = 3
    }

    /// <summary>
    /// Indicates the access authentication type
    /// </summary>
    public enum AccessAuthenticationTypeEnum : int
    {
        /// <summary>
        /// Indicates that none access authentication type is used
        /// </summary>
        None = 0,
        /// <summary>
        /// Indicates that the access authentication type by password is used
        /// </summary>
        Password = 1,
        /// <summary>
        /// Indicates that the access authentication type by fingerprint is used
        /// </summary>
        FingerPrint = 2
    }

    /// <summary>
    /// Indicates the type of access profile rule
    /// </summary>
    public enum AccessProfileRuleTypeEnum : int
    {
        /// <summary>
        /// Indicates the access profile rule by 'quantity'
        /// </summary>
        ByQuantity = 0,
        /// <summary>
        /// Indicates the access profile rule by 'shift'
        /// </summary>
        ByShift = 1,
        /// <summary>
        /// Indicates the access profile rule by 'working time'
        /// </summary>
        ByWorkingTime = 2
    }

    /// <summary>
    /// Equipment Model (hardware) indication
    /// </summary>
    public enum ProtocolTypeEnum : int
    {
        /// <summary>
        /// Indicates that the equipment is a BioPointII
        /// </summary>
        BioPointII = 1,
        /// <summary>
        /// Indicates that the equipment is a MicropointIP
        /// </summary>
        MicropointIP = 2,
        /// <summary>
        /// Indicates that the equipment is a MicropointXP
        /// </summary>
        MicropointXP = 3,
        /// <summary>
        /// Indicates that the equipment is a DMPCop
        /// </summary>
        DMPCop = 4,
        /// <summary>
        /// Indicates that the equipment is a CentralIO
        /// </summary>
        CentralIO = 5,
        /// <summary>
        /// Indicates that the equipment is a PrintPointII
        /// </summary>
        PrintPointII = 6,
        /// <summary>
        /// Indicates that the equipment is a TerminalCM3
        /// </summary>
        TerminalCM3 = 7,
        /// <summary>
        /// Indicates that the equipment is a MicropointS
        /// </summary>
        MicropointS = 8,
        /// <summary>
        /// Indicates that the equipment is a BioPointK
        /// </summary>
        BioPointK = 9,
        /// <summary>
        /// Indicates that the equipment is a ConcentradoraCM3
        /// </summary>
        ConcentradoraCM3 = 10,
        /// <summary>
        /// Indicates that the equipment is a Miniprint
        /// </summary>
        Miniprint = 12,
        /// <summary>
        /// Indicates that the equipament is a FaceAccess
        /// </summary>
        FaceAccess = 14,
        /// <summary>
        /// Indicates that the equipament is a PrintPointIII
        /// </summary>
        PrintPointIII = 15,
        /// <summary>
        /// Indicates that the equipament is a SmartFace
        /// </summary>
        SmartFace = 16,
        /// <summary>
        /// Indicates that the equipament is a camera ip
        /// </summary>
        CameraIP = 17,
        /// <summary>
        /// Indicates that the equipament is a ZKAIPush
        /// </summary>
        ZKAIPush = 18,
        /// <summary>
        /// Indicates that the equipament is a DMPCM
        /// </summary>
        DMPCM = 19
    }

    /// <summary>
    /// Equipment Model (hardware) indication
    /// </summary>
    public enum EquipmentModelTypeEnum : int
    {
        /// <summary>
        /// Indicates that the equipment is a BioPointII
        /// </summary>
        BioPointII = 1,
        /// <summary>
        /// Indicates that the equipment is a MD5704
        /// </summary>
        MD5704 = 2,
        /// <summary>
        /// Indicates that the equipment is a MicropointIP
        /// </summary>
        MicropointIP = 3,
        /// <summary>
        /// Indicates that the equipment is a MD2707
        /// </summary>
        MD2707 = 4,
        /// <summary>
        /// Indicates that the equipment is a MicropointXP
        /// </summary>
        MicropointXP = 5,
        /// <summary>
        /// Indicates that the equipment is a MD3707
        /// </summary>
        MD3707 = 6,
        /// <summary>
        /// Indicates that the equipment is a DMPCop
        /// </summary>
        DMPCop = 7,
        /// <summary>
        /// Indicates that the equipment is a DoorControler
        /// </summary>
        DoorControler = 8,
        /// <summary>
        /// Indicates that the equipment is an CentralIO
        /// </summary>
        CentralIO = 9,
        /// <summary>
        /// Indicates that the equipment is a CentralMDA6702
        /// </summary>
        CentralMDA6702 = 10,
        /// <summary>
        /// Indicates that the equipment is a PrintPointII
        /// </summary>
        PrintPointII = 11,
        /// <summary>
        /// Indicates that the equipment is a MDREP
        /// </summary>
        MDREP = 12,
        /// <summary>
        /// Indicates that the equipment is a TerminalCM3
        /// </summary>
        TerminalCM3 = 13,
        /// <summary>
        /// Indicates that the equipment is a Terminal6701
        /// </summary>
        Terminal6701 = 14,
        /// <summary>
        /// Indicates that the equipment is a MicropointS
        /// </summary>
        MicropointS = 15,
        /// <summary>
        /// Indicates that the equipment is a MD2701
        /// </summary>
        MD2701 = 16,
        /// <summary>
        /// Indicates that the equipment is a BioPointK
        /// </summary>
        BioPointK = 17,
        /// <summary>
        /// Indicates that the equipment is a MD5704Sensor5
        /// </summary>
        MD5704Sensor5 = 18,
        /// <summary>
        /// Indicates that the equipment is a ConcentradoraCM3
        /// </summary>
        ConcentradoraCM3 = 19,
        /// <summary>
        /// Indicates that the equipment is a ConcentradoraMDA6701
        /// </summary>
        ConcentradoraMDA6701 = 20,
        /// <summary>
        /// Indicates that the equipment is a Miniprint
        /// </summary>
        Miniprint = 23,
        /// <summary>
        /// Indicates that the equipment is a MD0705
        /// </summary>
        MD0705 = 24,
        /// <summary>
        /// Indicates that the equipment is a FaceAccess
        /// </summary>
        FaceAccess = 27,
        /// <summary>
        /// Indicates that the equipment is a FaceAccess for Madis (MD5711F)
        /// </summary>
        MD5711F = 28,
        /// <summary>
        /// Indicates that the equipment is a PrintPointIII
        /// </summary>
        PrintPointIII = 29,
        /// <summary>
        /// Indicates that the equipment is a MDEVO
        /// </summary>
        MDEVO = 30,
        /// <summary>
        /// The face access ii
        /// </summary>
        SmartFace = 31,
        /// <summary>
        /// The MD5712F
        /// </summary>
        MD5712F = 32,
        /// <summary>
        /// Indicates that the equipment is a CameraIP
        /// </summary>
        CameraIP = 33,
        /// <summary>
        /// Indicates that the equipment is a ZKAIPush
        /// </summary>
        ZKAIPush = 34,
        /// <summary>
        /// Indicates that the equipment is a ZKAIPush for Madis
        /// </summary>
        MDZKAIPush = 35,
        /// <summary>
        /// Indicates that the equipment is a DMP CM
        /// </summary>
        DMPCM = 36,
        /// <summary>
        /// Indicates that the equipment is a MD CM
        /// </summary>
        MDCM = 37
    }

    /// <summary>
    /// Second Relay Type enum
    /// </summary>
    public enum SecondRelayTypeEnum : int
    {
        /// <summary>
        /// Indicates that the second relay never triggers
        /// </summary>
        NeverTriggers = 0,
        /// <summary>
        /// Indicates that the second relay triggers in cases of violations
        /// </summary>
        TriggersInViolation = 1,
        /// <summary>
        /// Indicates that the second relay triggers in cases of blocked accesses
        /// </summary>
        TriggersInBlockedAccesses = 2
    }

    /// <summary>
    /// Digit Check Type enum
    /// </summary>
    public enum CheckerTypeEnum : int
    {
        /// <summary>
        /// Indicates that the equipment won't check the credential check digit
        /// </summary>
        NoChecking = 0,
        /// <summary>
        /// Indicates that the equipment will check the credential check digit with the type module 11
        /// </summary>
        ModuleEleven = 1,
        /// <summary>
        /// Indicates that the equipment will check the credential check digit with the type module 10
        /// </summary>
        ModuleTen = 2
    }

    /// <summary>
    /// Indicates the type of fingerprints
    /// </summary>
    [DataContract]
    public enum FingerprintTypeEnum : int
    {
        /// <summary>
        /// Indicates the left pinky fingerprint
        /// </summary>
        LeftPinkyFinger = 0,
        /// <summary>
        /// Indicates the left ring fingerprint
        /// </summary>
        LeftRingFinger = 1,
        /// <summary>
        /// Indicates the left middle fingerprint
        /// </summary>
        LeftMiddleFinger = 2,
        /// <summary>
        /// Indicates the left index fingerprint
        /// </summary>
        LeftIndexFinger = 3,
        /// <summary>
        /// Indicates the left thumb fingerprint
        /// </summary>
        LeftThumb = 4,
        /// <summary>
        /// Indicates the right pinky fingerprint
        /// </summary>
        RightPinkyFinger = 5,
        /// <summary>
        /// Indicates the right ring fingerprint
        /// </summary>
        RightRingFinger = 6,
        /// <summary>
        /// Indicates the right middle fingerprint
        /// </summary>
        RightMiddleFinger = 7,
        /// <summary>
        /// Indicates the right index fingerprint
        /// </summary>
        RightIndexFinger = 8,
        /// <summary>
        /// Indicates the right thumb fingerprint
        /// </summary>
        RightThumb = 9,
        /// <summary>
        /// Indicates the default fingerprint
        /// </summary>
        [HiddenEnum]
        [SwaggerHiddenEnum]
        Default = 10,
        /// <summary>
        /// Indicates the alternative fingerprint
        /// </summary>
        [HiddenEnum]
        [SwaggerHiddenEnum]
        Alternative = 11
    }

    /// <summary>
    /// Indicates the type AccessInquiry
    /// </summary>
    [DataContract]
    public enum AccessInquiryTypeEnum : int
    {
        [EnumMember]
        Card = 0,
        [EnumMember]
        SmartCard = 1,
        [EnumMember]
        Password = 2,
        [EnumMember]
        Master = 3,
        [EnumMember]
        Keyboard = 4,
        [EnumMember]
        Fingerprint = 5,
        [EnumMember]
        AccessButton = 6,
        [EnumMember]
        RemoteFingerprint = 7,
        [EnumMember]
        AllowAnAccess = 8
    }

    /// <summary>
    /// The possible events raised by the access validation (in full)
    /// </summary>
    [DataContract]
    public enum AccessValidationStatusTypeEnum : int
    {
        /// <summary> 
        /// Group Not Exists 
        /// TEMPORARY EVENT, IT IS NOT LOGGED.
        /// </summary> 
        [HiddenEnum]
        [EnumMember]
        GroupNotExists = 0, //TODO: Esse código não está definido
        /// <summary> 
        /// Access Granted 
        /// </summary> 
        [EnumMember]
        AccessGranted = 1,
        /// <summary> 
        /// Shift Has No Time Slot - ENUM CAN NOT BE REACHED IN THE  ACCESS VALIDATION, so it was removed
        /// </summary> 
        //ShiftHasNoTimeSlot = 2,
        /// <summary> 
        /// Out of time slot 
        /// </summary> 
        [EnumMember]
        OutOfTimeSlot = 3,
        /// <summary> 
        /// Exceeded Capacity Access Time Slot 
        /// </summary> 
        [EnumMember]
        ExceededCapAccessTimeSlot = 4,
        /// <summary> 
        /// Access Quantity Exceeded 
        /// </summary> 
        [EnumMember]
        AccessQuantityExceeded = 5,
        /// <summary> 
        /// Credential Blocked 
        /// </summary> 
        [EnumMember]
        CredentialBlocked = 6,
        /// <summary> 
        /// Reentry Not Allowed 
        /// </summary> 
        [EnumMember]
        ReentryNotAllowed = 7,
        /// <summary> 
        /// Access Not Completed 
        /// </summary> 
        [EnumMember]
        AccessNotCompleted = 8,
        /// <summary> 
        /// Credential Not Exists 
        /// </summary> 
        [EnumMember]
        CredentialNotExists = 9,
        /// <summary> 
        /// Access Completed 
        /// </summary> 
        [EnumMember]
        AccessCompleted = 10,
        /// <summary> 
        /// Concurrent Access Not Allowed 
        /// </summary> 
        [EnumMember]
        ConcurrentAccessNotAllowed = 11,
        /// <summary> 
        /// Access Finished In Batch 
        /// </summary> 
        [EnumMember]
        AccessFinishedInBatch = 12,
        /// <summary> 
        /// Credential Expired 
        /// </summary> 
        [EnumMember]
        CredentialExpired = 13,
        /// <summary> 
        /// Area Not Exists 
        /// TEMPORARY EVENT, IT IS NOT LOGGED.
        /// </summary> 
        [HiddenEnum]
        [EnumMember]
        AreaNotExists = 17,
        /// <summary> 
        /// Area Blocked 
        /// </summary> 
        [EnumMember]
        AreaBlocked = 18,
        /// <summary> 
        /// Area Capacity Exceeded 
        /// </summary> 
        [EnumMember]
        AreaCapacityExceeded = 19,
        /// <summary> 
        /// Access Control Rule Not Exists 
        /// </summary> 
        [EnumMember]
        AccessControlRuleNotExists = 20,
        /// <summary> 
        /// Reentry Allowed 
        /// </summary> 
        [EnumMember]
        EntityReentryCompleted = 21,
        /// <summary> 
        /// Shift Not Registered 
        /// </summary> 
        [EnumMember]
        ShiftNotRegistered = 22,
        /// <summary> 
        /// Access Completed By Authorization 
        /// </summary> 
        [EnumMember]
        AccessCompletedByAuthorization = 23,
        /// <summary> 
        /// Blocked Invalid Sequence Areas 
        /// </summary> 
        [EnumMember]
        BlockedInvalidSequenceAreas = 24,
        /// <summary> 
        /// Reentry Allowed By Area 
        /// </summary> 
        [EnumMember]
        AreaReentryCompleted = 25,
        /// <summary> 
        /// Finished Access Invalid Seq Areas 
        /// </summary> 
        [EnumMember]
        FinishedAccessInvalidSeqAreas = 26,
        /// <summary> 
        /// Master Access Completed 
        /// </summary> 
        [EnumMember]
        MasterAccessCompleted = 27,
        /// <summary> 
        /// Not Registered Working Time 
        /// </summary> 
        [EnumMember]
        NotRegisteredWorkingTime = 28,
        /// <summary> 
        /// Credential Not Associated To Person 
        /// </summary> 
        [EnumMember]
        CredentialNotAssociatedToPerson = 29,
        /// <summary> 
        /// Visitor Validation Failure 
        /// </summary> 
        [EnumMember]
        VisitorValidationFailure = 30,
        /// <summary> 
        /// Time Registry Completed
        /// </summary> 
        [EnumMember]
        TimeRegistryCompleted = 31,
        /// <summary> 
        /// Punch Registered in Batch 
        /// </summary> 
        [EnumMember]
        TimeRegisteredInBatch = 32,
        /// <summary> 
        /// Time Registry Granted 
        /// </summary> 
        [EnumMember]
        TimeRegistryGranted = 33,
        /// <summary> 
        /// Punch Registered By Authorization 
        /// </summary> 
        [EnumMember]
        TimeRegistryCompletedByAuthorization = 34,
        /// <summary> 
        /// Visitor Attempt To Register Time 
        /// </summary> 
        [EnumMember]
        VisitorAttemptToRegisterTime = 35,
        /// <summary> 
        /// Time Registry Rule Not Exists 
        /// </summary> 
        [EnumMember]
        TimeRegistryRuleNotExists = 36,
        /// <summary> 
        /// Prevented Access Both 
        /// </summary> 
        [EnumMember]
        PreventedAccessBoth = 37,
        /// <summary> 
        /// Password Not Exists 
        /// </summary> 
        [EnumMember]
        PasswordNotExists = 38,
        /// <summary> 
        /// Fingerprint Not Registered 
        /// </summary> 
        [EnumMember]
        FingerprintNotRegistered = 39,
        /// <summary> 
        /// Fingerprint Doesnt Match 
        /// </summary> 
        [EnumMember]
        FingerprintDoesntMatch = 40,
        /// <summary> 
        /// Module Failure 
        /// TEMPORARY EVENT, IT IS NOT LOGGED.
        /// </summary> 
        [HiddenEnum]
        [EnumMember]
        ModuleFailure = 41,
        /// <summary> 
        /// Allowed Vehicle 
        /// TEMPORARY EVENT, IT IS NOT LOGGED.
        /// </summary> 
        //[HiddenEnum]
        [EnumMember]
        AllowedVehicle = 42,
        /// <summary> 
        /// Vehicle Allowed In Batch 
        /// TEMPORARY EVENT, IT IS NOT LOGGED.
        /// </summary> 
        //[HiddenEnum]
        [EnumMember]
        VehicleAllowedInBatch = 43,
        /// <summary> 
        /// Not Registered Vehicle 
        /// TEMPORARY EVENT, IT IS NOT LOGGED.
        /// </summary> 
        //[HiddenEnum]
        [EnumMember]
        NotRegisteredVehicle = 44,
        /// <summary> 
        /// Finished Vehicle 
        /// TEMPORARY EVENT, IT IS NOT LOGGED.
        /// </summary> 
        [HiddenEnum]
        [EnumMember]
        FinishedVehicle = 45,
        /// <summary> 
        /// Not Finished Vehicle 
        /// TEMPORARY EVENT, IT IS NOT LOGGED.
        /// </summary> 
        [HiddenEnum]
        [EnumMember]
        NotFinishedVehicle = 46,
        /// <summary> 
        /// Token Invalid Password 
        /// TEMPORARY EVENT, IT IS NOT LOGGED.
        /// </summary> 
        [HiddenEnum]
        [EnumMember]
        TokenInvalidPassword = 47,
        /// <summary> 
        /// Token Password Already Used 
        /// TEMPORARY EVENT, IT IS NOT LOGGED.
        /// </summary> 
        [HiddenEnum]
        [EnumMember]
        TokenPasswordAlreadyUsed = 48,
        /// <summary> 
        /// Blocked Token 
        /// TEMPORARY EVENT, IT IS NOT LOGGED.
        /// </summary> 
        [HiddenEnum]
        [EnumMember]
        BlockedToken = 49,
        /// <summary> 
        /// Access Depends On Token 
        /// TEMPORARY EVENT, IT IS NOT LOGGED.
        /// </summary> 
        [HiddenEnum]
        [EnumMember]
        AccessDependsOnToken = 50,
        /// <summary> 
        /// Validation Error Of Token 
        /// TEMPORARY EVENT, IT IS NOT LOGGED.
        /// </summary> 
        [HiddenEnum]
        [EnumMember]
        ValidationErrorOfToken = 51,
        /// <summary> 
        /// Time Range Not Allowed 
        /// </summary> 
        [EnumMember]
        TimeRangeNotAllowed = 52,
        /// <summary> 
        /// Visit Blocked 
        /// </summary> 
        [EnumMember]
        VisitBlocked = 53,
        /// <summary> 
        /// Exceed Quantity Access Person Group 
        /// </summary> 
        [EnumMember]
        ExceedQtAccessPersonGroup = 54,
        /// <summary> 
        /// Blocked By Day off 
        /// </summary> 
        [EnumMember]
        BlockedByDayoff = 55,
        /// <summary> 
        /// Original Credential Not Allowed 
        /// </summary> 
        [EnumMember]
        OrigCredentialNotAllowed = 56,
        /// <summary> 
        /// Person Block Attempt Original Provisory 
        /// </summary> 
        [EnumMember]
        PersonBlockAttemptOrigProvisory = 57,
        /// <summary> 
        /// Use equipment with safe
        /// </summary> 
        [EnumMember]
        UseEquipmentWithSafe = 58,
        /// <summary> 
        /// Access Not Finished In Batch 
        /// </summary> 
        [EnumMember]
        AccessNotFinishedInBatch = 59,
        /// <summary> 
        /// Access Not Completed by an invalid password
        /// </summary>
        [EnumMember]
        InvalidPassword = 60,
        /// <summary> 
        /// Time Registry Granted By Authorization 
        /// </summary> 
        [EnumMember]
        TimeRegistryGrantedByAuthorization = 61,
        /// <summary> 
        /// Access Granted by Exceptional Authorization
        /// TEMPORARY EVENT, IT IS NOT LOGGED.
        /// </summary> 
        [EnumMember]
        [HiddenEnum]
        AccessGrantedByAuthorization = 62,
        /// <summary> 
        /// Reentry Granted for person or credential
        /// TEMPORARY EVENT, IT IS NOT LOGGED.
        /// </summary> 
        [HiddenEnum]
        [EnumMember]
        EntityReentryGranted = 63,
        /// <summary> 
        /// Reentry granted for area
        /// TEMPORARY EVENT, IT IS NOT LOGGED.
        /// </summary> 
        [HiddenEnum]
        [EnumMember]
        AreaReentryGranted = 64,
        /// <summary> 
        /// Access Not Completed In Batch by an invalid password
        /// </summary>
        [EnumMember]
        InvalidPasswordInBatch = 65,
        /// <summary> 
        /// Blocked By Multiple Situation 
        /// </summary> 
        [EnumMember]
        BlockedByMultipleSituation = 66,
        /// <summary> 
        /// Access Completed By Authorizer 
        /// </summary> 
        [EnumMember]
        AccessCompletedByAuthorizer = 67,
        /// <summary> 
        /// Authorizer Not Associated To Person
        /// </summary> 
        [EnumMember]
        AuthorizerNotAssociatedToPerson = 68,
        /// <summary> 
        /// Equipment Doesn't Support Authorizer 
        /// </summary> 
        [EnumMember]
        EquipmentNotSupportsAuthorizer = 69,
        /// <summary> 
        /// Access Requires Authorizer
        /// TEMPORARY EVENT, IT IS NOT LOGGED.
        /// </summary> 
        [HiddenEnum]
        [EnumMember]
        AccessRequiresAuthorizer = 70,
        /// <summary> 
        /// Access Granted By Authorizer 
        /// </summary>        
        [EnumMember]
        AccessGrantedByAuthorizer = 71,
        /// <summary> 
        /// Authorizer not found.
        /// </summary> 
        [EnumMember]
        AuthorizerNotFound = 72,
        /// <summary>
        /// The access manual inserted by the web application
        /// </summary>
        [EnumMember]
        AccessManual = 73,
        /// <summary>
        /// Invalid function for authorization exceptional on equipment
        /// </summary>
        [EnumMember]
        InvalidFunction = 74,
        /// <summary>
        /// Access denied for sortition
        /// </summary>
        [EnumMember]
        AccessDeniedBySotition = 75,
        /// <summary>
        /// Access allow with sortition
        /// </summary>
        [EnumMember]
        AccessGrantedBySotition = 76,
        /// <summary>
        /// Blocked by precedence restriction
        /// </summary>
        [EnumMember]
        BlockedByPrecedenceRestriction = 77,
        /// <summary>
        /// Free exit
        /// </summary>
        [EnumMember]
        FreeExit,
        /// <summary>
        /// Blocked by Working Time
        /// </summary>
        [EnumMember]
        BlockedWorkingTime

    }

    /// <summary>
    /// Indicates status of output digital.
    /// </summary>
    public enum OutputDigitalStatusTypeEnum : int
    {
        Disable = 2,
        Deactivated = 4,
        Activated = 5
    }

    public enum InputDigitalStatusTypeEnum : int
    {
        Disable = 2,
        Normal = 4,
        Alarm = 5
    }

    /// <summary>
    /// Indicates how the equipment will work
    /// </summary>
    public enum EquipmentModeTypeEnum : int
    {
        /// <summary>
        /// Indicates that equipment mode value is 'Batch' (only for Biopoint, CIO, CM3, DMP COP, Micropoint IP and Micropoint XP)
        /// </summary>
        Batch = 0,
        /// <summary>
        /// Indicates that equipment mode value is 'RealTimeWithoutPassword' (only for Micropoint IP)
        /// </summary>
        RealTimeWithoutPassword = 1,
        /// <summary>
        /// Indicates that equipment mode value is 'RealTimeWithPassword' (only for Micropoint IP)
        /// </summary>
        RealTimeWithPassword = 2,
        /// <summary>
        /// ONLY FOR COMMUNICATION PURPOSE.
        /// Indicates that equipment mode value is 'Realtime' (only for Biopoint, CIO, CM3, DMP COP, Micropoint IP and Micropoint XP)
        /// </summary>
        Realtime = 3,
        /// <summary>
        /// Indicates that equipment mode value is 'Deactivated' (only for CIO and CM3)
        /// </summary>
        Deactivated = 4,
        /// <summary>
        /// Indicates that equipment mode value is 'BootLoader' (only for CIO and CM3)
        /// </summary>
        BootLoader = 5,
        /// <summary>
        /// Indicates that equipment mode value is 'Normal' (only for CIO, CM3, MiniPrint and PrintPoint II)
        /// </summary>
        Normal = 6,
        /// <summary>
        /// Indicates that equipment mode value is 'Alarm' (only for CIO)
        /// </summary>
        Alarm = 7,
        /// <summary>
        /// Indicates that equipment mode value is 'Maintenance' (only for MiniPrint and PrintPoint II)
        /// </summary>
        Maintenance = 8
    }

    /// <summary>
    /// Access Type for a PersonSituation
    /// </summary>
    [DataContract]
    public enum AccessSituationTypeEnum : int
    {
        /// <summary>
        /// Indicates that the situation is valid
        /// </summary>
        [EnumMember]
        Valid = 1,
        /// <summary>
        /// Indicates that the situation is blocked
        /// </summary>
        [EnumMember]
        Blocked = 2,
        /// <summary>
        /// Indicates that the situation is multiple and have one or many periods
        /// </summary>
        [EnumMember]
        Multiple = 3,
        /// <summary>
        /// Indicates that the situation is blocked by period
        /// </summary>
        [EnumMember]
        PeriodBlocked = 4,
        /// <summary>
        /// Indicates that the situation is valid by period
        /// </summary>
        [EnumMember]
        PeriodValid = 5
    }

    /// <summary>
    /// Indicates the reference filter type
    /// </summary>
    public enum ReferenceTypeEnum : int
    {
        /// <summary>
        /// Whether the reference filter is set for people
        /// </summary>
        Person = 1,
        /// <summary>
        /// Whether the reference filter is set for visitors
        /// </summary>
        Visitor = 2
    }

    /// <summary>
    /// Indicates the reference filter type
    /// </summary>
    public enum MovimentAreaEnum : int
    {
        /// <summary>
        /// Whether the reference filter is set for people
        /// </summary>
        NoMoviment = 1,
        /// <summary>
        /// Whether the reference filter is set for visitors
        /// </summary>
        Moviment = 2
    }

    /// <summary>
    /// Indicates which is the manufacturer of the biometric module
    /// </summary>
    public enum BiometricModuleTypeEnum : int
    {
        /// <summary>
        /// Indicates that the manufacturer is Sagem
        /// </summary>
        Sagem = 0,
        /// <summary>
        /// Indicates that the manufacturer is Suprema
        /// </summary>
        Suprema = 1,
        /// <summary>
        /// Indicates that the manufacturer is NeoKoros
        /// </summary>
        [SwaggerHiddenEnum]
        NeoKoros = 2,
        /// <summary>
        /// Indicates that the manufacturer is Siemens
        /// </summary>
        [SwaggerHiddenEnum]
        Siemens = 3,
        /// <summary>
        /// Indicates that the manufacturer is Testech 317
        /// </summary>
        [SwaggerHiddenEnum]
        Testech317 = 4,
        /// <summary>
        /// Indicates that the manufacturer is Testech 320
        /// </summary>
        [SwaggerHiddenEnum]
        Testech320 = 5,
        /// <summary>
        /// Indicates that the manufacturer is Virdi
        /// </summary>
        [SwaggerHiddenEnum]
        Virdi = 6,
        /// <summary>
        /// Indicates that the manufacturer is Inttelix
        /// </summary>
        Inttelix = 7,
        /// <summary>
        /// Indicates that the manufacturer is Secukey
        /// </summary>
        SecuKey = 8,
        /// <summary>
        /// Indicates that the manufacturer is Nitgen
        /// </summary>
        [SwaggerHiddenEnum]
        Nitgen = 9,
        /// <summary>
        /// Indicates that the manufacturer is zk MB360 Face Algorithm 7
        /// </summary>
        ZkMb360FaceAlg7 = 10,
        /// <summary>
        /// Indicates that the manufacturer is zk MB360 bio
        /// </summary>
        [SwaggerHiddenEnum]
        ZkMb360Bio = 11,
        /// <summary>
        ///Indicates that the manufacturer is zk push face, using in Smart Face S and MD 5713F device.
        /// </summary>
        ZKAIPushFace = 12,
        /// <summary>
        /// Indicates that the manufacturer is  zk push bio, using in Smart Face S and MD 5713F device.
        /// </summary>
        [SwaggerHiddenEnum]
        ZKAIPushBio = 13,
        /// <summary>
        /// Indicates that the manufacturer is ZK InBio
        /// </summary>
        [SwaggerHiddenEnum]
        ZKInBio = 14,
        /// <summary>
        /// Indicates that the manufacturer is  zk push palm, using in Smart Face S and MD 5713F device.
        /// </summary>
        [SwaggerHiddenEnum]
        ZKAIPushPalm = 15

    }

    /// <summary>
    /// Specifies the access type in batch mode
    /// </summary>
    public enum BatchAccessTypeEnum : int
    {

        /// <summary>
        /// Indicates that batch access value is 'AccessDenied'
        /// </summary>
        AccessDenied = 1,
        /// <summary>
        /// Indicates that batch access value is 'AccessGrantedWithoutRegister'
        /// </summary>
        AccessGrantedWithoutRegister = 7,
        /// <summary>
        /// Indicates that batch access value is 'AccessGrantedWithRegisterGrantedWhenFull'
        /// </summary>
        AccessGrantedWithRegisterGrantedWhenFull = 0,
        /// <summary>
        /// Indicates that batch access value is 'AccessGrantedWithRegisterDeniedWhenFull'
        /// </summary>
        AccessGrantedWithRegisterDeniedWhenFull = 8,
        /// <summary>
        /// Indicates that batch access value is 'InquireCredentialList'
        /// </summary>
        InquireCredentialList = 2,
        /// <summary>
        /// Indicates that batch access value is 'InquireCredentialListAndCheckPassword'
        /// </summary>
        InquireCredentialListAndCheckPassword = 3,
        /// <summary>
        /// Indicates that batch access value is 'InquireCredentialListAndCheckFingeprintMandatorily'
        /// </summary>
        InquireCredentialListAndCheckFingeprintMandatorily = 4,
        /// <summary>
        /// Indicates that batch access value is 'InquireCredentialListAndCheckFingeprintIfExists'
        /// </summary>
        InquireCredentialListAndCheckFingeprintIfExists = 5,
        /// <summary>
        /// Indicates that batch access value is 'InquireCredentialListAndCheckPasswordAndFingeprint'
        /// </summary>
        InquireCredentialListAndCheckPasswordAndFingeprint = 6,
        /// <summary>
        /// Indicates that batch access value is 'InquireCredentialListAndCheckPasswordAndFingeprint'
        /// </summary>
        None = 9
    }

    public enum ActuationTypeEnum : int
    {
        /// <summary>
        /// Indicates that actuation type value is 'None'
        /// </summary>
        None = 1,
        /// <summary>
        /// Indicates that actuation type value is 'NoneWithSafe'
        /// </summary>
        NoneWithSafe = 2,
        /// <summary>
        /// Indicates that actuation type value is 'ControlsVehicleWithoutActuation'
        /// </summary>
        ControlsVehicleWithoutActuation = 3,
        /// <summary>
        /// Indicates that actuation type value is 'DoorLockWithoutSensor'
        /// </summary>
        DoorLockWithoutSensor = 4,
        /// <summary>
        /// Indicates that actuation type value is 'DoorLockWithoutSensorWithSafe'
        /// </summary>
        DoorLockWithoutSensorWithSafe = 5,
        /// <summary>
        /// Indicates that actuation type value is 'DoorLockWithSensor'
        /// </summary>
        DoorLockWithSensor = 6,
        /// <summary>
        /// Indicates that actuation type value is 'DoubleDoorLock'
        /// </summary>
        DoubleDoorLock = 7,
        /// <summary>
        /// Indicates that actuation type value is 'ControlsVehicleWithGate'
        /// </summary>
        ControlsVehicleWithGate = 8,
        /// <summary>
        /// Indicates that actuation type value is 'EntranceTurnstile'
        /// </summary>
        EntranceTurnstile = 9,
        /// <summary>
        /// Indicates that actuation type value is 'EntranceTurnstileWithFreeExit'
        /// </summary>
        EntranceTurnstileWithFreeExit = 10,
        /// <summary>
        /// Indicates that actuation type value is 'ExitTurnstile'
        /// </summary>
        ExitTurnstile = 11,
        /// <summary>
        /// Indicates that actuation type value is 'UnidirectionalTurnstile'
        /// </summary>
        UnidirectionalTurnstile = 12,
        /// <summary>
        /// Indicates that actuation type value is 'UnidirectionalTurnstileWithSafe'
        /// </summary>
        UnidirectionalTurnstileWithSafe = 13,
        /// <summary>
        /// Indicates that actuation type value is 'BidirectionalTurnstile'
        /// </summary>
        BidirectionalTurnstile = 14,
        /// <summary>
        /// Indicates that actuation type value is 'BidirectionalTurnstileWithSafe'
        /// </summary>
        BidirectionalTurnstileWithSafe = 15,
        /// <summary>
        /// Indicates that actuation type value is 'BidirectionalTurnstileDirectionDependingOnReader'
        /// </summary>
        BidirectionalTurnstileDirectionDependingOnReader = 16,
        /// <summary>
        /// Indicates that actuation type value is 'BidirectionalTurnstileDirectionNotDependingOnReader'
        /// </summary>
        BidirectionalTurnstileDirectionNotDependingOnReader = 17,
        /// <summary>
        /// Indicates that actuation type value is 'BidirectionalTurnstileDirectionDependingOnCardPassage'
        /// </summary>
        BidirectionalTurnstileDirectionDependingOnCardPassage = 18,
        /// <summary>
        /// Indicates that actuation type value is 'TurnstileWithSafeAndThreeReaders'
        /// </summary>
        TurnstileWithSafeAndThreeReaders = 19,
        /// <summary>
        /// Indicates that actuation type value is 'TurnstileWithSafeAndTwoReaders'
        /// </summary>
        TurnstileWithSafeAndTwoReaders = 20,
        /// <summary>
        /// The door lock with safe without sensor
        /// </summary>
        DoorLockWithSafeWithoutSensor = 21,
        /// <summary>
        /// The door lock with safe with sensor
        /// </summary>
        DoorLockWithSafeWithSensor = 22
    }

    /// <summary>
    /// Type of the working time
    /// </summary>
    public enum WorkingTimeTypeEnum : int
    {
        /// <summary>
        /// Indicates that working time has 'Weekly' value
        /// </summary>
        Weekly = 1,
        /// <summary>
        /// Indicates that working time has 'Monthly' value
        /// </summary>
        Monthly = 2,
        /// <summary>
        /// Indicates that working time has 'Periodically' value
        /// </summary>
        Periodically = 3
    }

    /// <summary>
    /// Rules to provisional credential
    /// </summary>
    public enum ProvisionalCredentialRuleEnum : int
    {
        /// <summary>
        /// Passing original credentials, disassociates the provisional credential of the person
        /// </summary>
        PassingOriginalDisassociateProvisional = 1,

        /// <summary>
        /// Associating provisional credential to the person, prevents the use of the original credential
        /// </summary>
        AssociatingProvisionalPreventOriginal = 2,

        /// <summary>
        /// Blocks the person if she has an associated provisional credential and use the original credential
        /// </summary>
        BlockPersonIfUseOriginal = 3
    }

    /// <summary>
    /// Indicates the Technology used by a credential
    /// </summary>
    [DataContract]
    public enum TechnologyTypeEnum : int
    {
        /// <summary>
        /// Indicates that technology  has 'BarCode' value
        /// </summary>
        [EnumMember]
        BarCode = 1,
        /// <summary>
        /// Indicates that technology  has 'MagneticCard' value
        /// </summary>
        [EnumMember]
        MagneticCard = 2,
        /// <summary>
        /// Indicates that technology  has 'Proximity' value
        /// </summary>
        [EnumMember]
        Proximity = 3,
        /// <summary>
        /// Indicates that technology  has 'SmartCard' value
        /// </summary>
        [EnumMember]
        SmartCard = 4,
        /// <summary>
        /// Indicates that technology  has 'RFID' value
        /// </summary>
        [EnumMember]
        RFID = 5
    }

    /// <summary>
    /// Indicates the collect of the batch records types
    /// </summary>
    public enum CollectTypeEnum : int
    {
        /// <summary>
        /// Indicates that the equipment must operate in Realtime mode and must collect the batch records, only if 
        /// the equipment isn't sending any message to the software
        /// </summary>
        CollectWithPreferentialRealtime = 1,
        /// <summary>
        /// Indicates that the equipment must operate in Realtime mode and must not collect the batch records
        /// </summary>
        RealtimeWithoutCollect = 2
    }

    /// <summary>
    /// Specifies access direction types
    /// </summary>
    [DataContract]
    public enum AccessDirectionType
    {
        [EnumMember]
        Entrance = 1,
        [EnumMember]
        Exit = 2,
        [EnumMember]
        Both = 3,
        [SwaggerHiddenEnum]
        [EnumMember]
        SameOfInquiry = 4
    }

    /// <summary>
    /// Specifies access direction types
    /// </summary>
    [DataContract]
    public enum AccessDirectionTypeApi
    {
        [EnumMember]
        Entrance = 1,
        [EnumMember]
        Exit = 2,
    }

    /// <summary>
    /// Specifies the batch mode type that are in the list
    /// </summary>
    public enum BatchModeInListType
    {
        Block = 1,
        Release = 2,
        ReleaseWithFingerprint = 3,
        ReleaseWithFingerprintIfAny = 4,
        ReleaseWithFingerprintOrPassword = 5
    }

    /// <summary>
    /// Specifies the batch mode type that are in the list
    /// </summary>
    public enum VehiclePlateValidationType
    {
        None = 1,
        Brazil = 2
    }

    /// <summary>
    /// Specifies the batch mode type that are NOT in the list
    /// </summary>
    public enum BatchModeNotInListType
    {
        Block = 1,
        Release = 2
    }

    /// <summary>
    /// Specifies format reading type
    /// </summary>
    public enum FormatReadingTypeEnum
    {
        /// <summary>
        /// //Intercalado 2 de 5
        /// </summary>
        Interleaved25 = 1,

        // TODO: Não pode ter o nome da DIMEP! Alterar para o que? Sugestão no código da DIMEP: 
        //       Intercalado 2 de 5 DIMEP -> Intercalado 2 de 5 Especial. Analisar implicações para
        //       os recursos e os enums das telas!
        /// <summary>
        /// 2 de 5 Dimep
        /// </summary>
        Interleaved25_DIMEP = 2,

        /// <summary>
        /// 3 de 9
        /// </summary>
        Format_3_9 = 3,

        /// <summary>
        /// Magnético
        /// </summary>
        Magnetic = 4,

        /// <summary>
        /// ABA
        /// </summary>
        ABA = 5,

        /// <summary>
        /// Smart Card
        /// </summary>
        SmartCard = 6,

        /// <summary>
        /// Ean 13
        /// </summary>
        Ean13 = 7,

        /// <summary>
        /// Wiegand 26 bits
        /// </summary>
        Wiegand26 = 8,

        /// <summary>
        /// Wiegand 32 bits especial
        /// </summary>
        Wiegand32Special = 9,

        /// <summary>
        /// Wiegand 34 bits
        /// </summary>
        Wiegand34 = 10,

        /// <summary>
        /// Wiegand 35 bits
        /// </summary>
        Wiegand35 = 11,

        /// <summary>
        /// Wiegand 37 bits
        /// </summary>
        Wiegand37 = 12
    }

    /// <summary>
    /// Specifies format reading type
    /// </summary>
    public enum FormatReadingRegistrationNumberType
    {
        /// <summary>
        /// Reads the ID
        /// </summary>
        ReadID = 1,

        /// <summary>
        /// Reads the registration number in BCD.
        /// </summary>
        ReadInBCD = 2,

        /// <summary>
        /// Reads the registration number in Hexadecimal.
        /// </summary>
        ReadInHex = 3,
    }

    public enum FormatReadingRegistrationNumberNewType
    {
        /// <summary>
        /// The none
        /// </summary>
        None = 0,

        /// <summary>
        /// Reads the ID
        /// </summary>
        ReadID = 1,

        /// <summary>
        /// Reads the registration number in BCD.
        /// </summary>
        ReadInBCD = 2,

        /// <summary>
        /// The format special smart card1
        /// </summary>
        FormatSpecialSmartCard1 = 3,

        /// <summary>
        /// The format special smart card2
        /// </summary>
        FormatSpecialSmartCard2 = 4,

        /// <summary>
        /// Reads the registration number in Hexadecimal.
        /// </summary>
        ReadInHex = 5,
    }

    /// <summary>
    /// Specifies format reading type
    /// </summary>
    public enum PatternWiegand37TypeEnum
    {
        /// <summary>
        /// Pattern H10302
        /// </summary>
        H10302 = 1,

        /// <summary>
        /// Pattern H10304
        /// </summary>
        H10304 = 2
    }

    /// <summary>
    /// Specifies position reading type
    /// </summary>
    public enum PositionReadingTypeEnum
    {
        LeftSensor = 0,
        RightSensor = 1,
        Independent = 2,
        DownSensor = 3,
        UpSensor = 4
    }

    /// <summary>
    /// Specifies card customization type
    /// </summary>
    public enum CardCustomizationType
    {
        Micropoint = 1,

        Megapoint = 2
    }

    /// <summary>
    /// Specifies the number of input
    /// </summary>
    public enum InputNumberType
    {
        Disabled = 0,

        Entrance1 = 1,

        Entrance2 = 2
    }

    /// <summary>
    /// Specifies the number of exit
    /// </summary>
    public enum ExitNumberType
    {
        Disabled = 0,

        Exit1 = 1
    }

    /// <summary>
    /// Specifies the sensor input
    /// </summary>
    public enum SensorInputType
    {
        Door = 0,

        ReturnAccessCompleted = 1
    }

    /// <summary>
    /// Specifies the quiescent signal level for the sensor
    /// </summary>
    public enum QuiescentSignalType
    {
        Opened = 0,

        Short = 1
    }

    /// <summary>
    /// Specifies the actuation input type
    /// </summary>
    public enum ActuationInputType
    {
        AccessButton = 0,

        AnotherTerminalWithActuation = 1,

        AnotherTerminal = 2
    }

    /// <summary>
    /// Specifies the progress printer type
    /// </summary>
    public enum PrinterProgressTypeEnum
    {
        /// <summary>
        /// Indicates that progress printer has 'Short' value
        /// </summary>
        Short = 0,
        /// <summary>
        /// Indicates that progress printer has 'Medium' value
        /// </summary>
        Medium = 1,
        /// <summary>
        /// Indicates that progress printer has 'Long' value
        /// </summary>
        Long = 2
    }

    /// <summary>
    /// Specifies the cut of printer type
    /// </summary>
    public enum PrinterCutTypeEnum
    {
        /// <summary>
        /// Indicates that cut of printer has 'Partial' value
        /// </summary>
        Partial = 0,
        /// <summary>
        /// Indicates that cut of printer has 'Total' value
        /// </summary>
        Total = 1
    }


    /// <summary>
    /// Specifies the access type
    /// </summary>
    public enum IdentificationTypeEnum
    {
        /// <summary>
        /// Indicates that access type has 'Crendential' value
        /// </summary>
        DoesnotRequestPassword = 0,
        /// <summary>
        /// Indicates that access type has 'PIS' value
        /// </summary>
        OnlyPassword = 1
    }

    /// <summary>
    /// Specifies the access type
    /// </summary>
    public enum AccessTypeEnum
    {
        /// <summary>
        /// Indicates that access type has 'Crendential' value
        /// </summary>
        Credential = 0,
        /// <summary>
        /// Indicates that access type has 'DynamicDocument' value
        /// E.g. In Brazil is PIS but maybe other document depeding the country.
        /// </summary>
        DynamicDocument = 1
    }

    /// <summary>
    /// Specifies the authentication type
    /// </summary>
    public enum AuthenticationTypeEnum
    {
        /// <summary>
        /// Indicates that authentication type has 'Does not Request Password' value
        /// </summary>
        DoesnotRequestPassword = 0,
        /// <summary>
        /// Indicates that authentication type has 'Only Password' value
        /// </summary>
        OnlyPassword = 1,
        /// <summary>
        /// Indicates that authentication type has 'Only Biometric' value
        /// </summary>
        OnlyBiometric = 2,
        /// <summary>
        /// Indicates that authentication type has 'Biometric Or Password' value
        /// </summary>
        BiometricOrPassword = 3,
        /// <summary>
        /// Indicates that authentication type has 'Both' value
        /// </summary>
        Both = 4
    }

    /// <summary>
    ///  Specifies the authentication type for release an access with authentication
    /// </summary>
    public enum AuthenticationTypeEnumForReleaseAnAccessWithAuthentication
    {
        /// <summary>
        /// Indicates that access type has 'Crendential' value
        /// </summary>
        Credential = 0, //Credencial (via Leitor ou Teclado)

        /// <summary>
        /// Indicates that access type has 'Crendential With Biometric Authentication' value
        /// </summary>
        CredentialWithBiometricAuthentication = 1, ///Credencial Com Autenticação Biométrica

        /// <summary>
        /// Indicates that access type has 'SagemBiometric' value
        /// </summary>
        SagemBiometric = 2,

        /// <summary>
        /// Indicates that access type has 'SupremaBiometric' value
        /// </summary>
        SupremaBiometric = 3,

        /// <summary>
        /// Indicates that access type has 'VirdiBiometric' value
        /// </summary>
        VirdiBiometric = 4,

        /// <summary>
        /// Indicates that access type has 'SecukeyBiometric' value
        /// </summary>
        SecukeyBiometric = 5,

    }


    /// <summary>
    /// Specifies the behavior type
    /// </summary>
    public enum BehaviorTypeEnum
    {
        /// <summary>
        /// Indicates that behavior type has 'Always' value
        /// </summary>
        Always = 0,
        /// <summary>
        /// Indicates that behavior  type has 'Partial' value
        /// </summary>
        Partial = 1
    }

    /// <summary>
    /// Specifies the energy printer type
    /// </summary>
    public enum EnergyPrinterTypeEnum
    {
        /// <summary>
        /// Indicates that behavior type has 'normal' value
        /// </summary>
        Normal = 0,
        /// <summary>
        /// Indicates that behavior  type has '10% elevated' value
        /// </summary>
        TenPercentElevated = 1,
        /// <summary>
        /// Indicates that behavior  type has '10% reduced' value
        /// </summary>
        TenPercentReduced = 2
    }

    /// <summary>
    /// Specifies the command type
    /// </summary>
    public enum CommandTypeEnum
    {
        SetDateAndTime = 0,
        Configuration = 1,
        PartialConfiguration = 2,
        Activation = 3,
        Deactivation = 4,
        ActivateEmergencyRoute = 5,
        DeactivateEmergencyRoute = 6,
        DefaultMessage = 7,
        Functions = 8,
        Status = 9,
        ImmediateStatus = 10,
        ReleasesAccess = 11,
        SendFirmware = 12,
        LoadFirmwareTerminal = 13,
        MappingTerminals = 14,
        RepositioningMRPReadingPointer = 16,
        ClearSupervisors = 19,
        ClearFunctionsAndMessages = 20,
        ClearCredential = 21,
        ClearCredentialTotal = 22,
        ClearFingerprint = 23,
        ClearFingerprintTotal = 24,
        ClearPerson = 25,
        ClearPersonTotal = 26,
        ClearTemplateWithoutPisRegistered = 27,
        ClearListAndDigital = 28,
        Supervisor = 29,
        Credential = 31,
        CredentialTotal = 32,
        Person = 33,
        PersonTotal = 34,
        Fingerprint = 35,
        FingerprintTotal = 36,
        ListHeader = 37,
        StartManager = 38,
        StopManager = 39,
        SetOutputState = 40,
        RemoveNotifyForTemplateServerRestApi = 41,
        //Included command to release an access with authentication
        ReleasesAnAccessWithAuthentication = 42,
        GetTotalPeople = 43
    }

    /// <summary>
    /// Specifies the command status
    /// </summary>
    public enum CommandStatusEnum
    {
        Sent = 0,
        Processing = 1,
        Success = 2,
        Error = 3,
        Cancelled = 4,
        NotSupported = 5, //Only used at CommandEquipment class
        Cancelling = 6,
        Warning = 7, //Only used at CommandEquipment class
        [SwaggerHiddenEnum]
        ErrorAndResent = 8, //Only used at Command class
        [SwaggerHiddenEnum]
        SuccessWithWarning = 9, //Only used at Command class
        EquipmentNotUnderMaintenance = 10,
        ListCapacityReachedMaximumValue = 11
    }

    /// <summary>
    /// Specifies the command result status
    /// </summary>
    public enum CommandResultStatusEnum
    {
        NotProcessed = 0,
        Valid = 1,
        Success = 2,
        Error = 3,
        //warnings
        CredentialTechnologyNotSupported = 4,
        CredentialNumberNotSupported = 5,
        CredentialAuthenticationNotSupported = 6,
        AccessProfileNotExists = 7,
        CredentialNotAssociated = 8,
        PersonBlocked = 9,
        PersonWithoutPIS = 10,
        CredentialOutOfDate = 11,
        CredentialBlocked = 12,
        PersonWithoutFingerprint = 13,
        InvalidMessage = 14,
        ManagerAlreadyStarted = 15,
        ManagerAlreadyStopped = 16,
        PersonWithoutPassword = 17,
        PersonWithoutCpfOrInvalid = 18
    }

    /// <summary>
    /// Indicates the biometric command.
    /// </summary>
    public enum BiometricCommandEnum : int
    {
        UserIdentify = 0,
        UserAuthentication = 1,
        SecurityLevel = 2,
        ClearDigitals = 3,
        ReceiveDigital = 4,
        ClearUser = 5,
        HandleError = 6,
        Waiting = 7
    }

    /// <summary>
    /// Indicates the biometric status.
    /// </summary>
    public enum BiometricStatusEnum : int
    {
        Initialize = 0,
        Wait = 1,
        Command = 2,
        Communicating = 3,
        IdentifyDigital = 4,
        ToIdentification = 5
    }

    /// <summary>
    /// Specifies the command status
    /// </summary>
    public enum SupervisorPermissionEnum
    {
        TechnicalMenu = 0,
        ChangeDateAndTime = 1,
        ReplacePaperRoll = 2,
        OperationsWithPendrive = 3,
        TechnicalMenuAndChangeDateAndTime = 4,
        PinMaster = 5
    }

    /// <summary>
    /// Specifies message item types
    /// </summary>
    public enum MessageItemTypeEnum
    {
        DefaultMessage = 0,
        EntranceAccessGranted = 1,
        ExitAccessGranted = 2,
        InvalidPassword = 3,
        AccessGranted = 4,
        UserWithoutFingerprint = 5,
        SensorTurnedOff = 6,
        ModuleFailure = 7,
        BlockedByList = 8
    }

    #region Enums for the PrintPoint

    /// <summary>
    /// Indicates the power supply type.
    /// </summary>
    public enum PowerSupplyEnumType
    {
        ACPowerSupply = 0,
        BatteryPowerSupply = 1,
        NoBreak = 2
    }

    /// <summary>
    /// Indicates the battery level.
    /// </summary>
    public enum BatteryEnumLevel
    {
        Regular = 0,
        Small = 1,
        VerySmall = 2,
    }

    /// <summary>
    /// Indicates the bobbin state
    /// </summary>
    public enum BobbinStateEnumType
    {
        OK = 0,
        LittlePaper = 1,
        NoPaper = 2,
        VeryLittlePaper = 3,
    }

    /// <summary>
    /// Indicates the MRP state.
    /// </summary>
    public enum MRPStateEnumType
    {
        NormalOperation = 0,
        Unknown = 1,
        WithoutEmployer = 2,
        WithoutInitialActivation = 3,
        Error = 4,
        Initializing = 5,
    }

    /// <summary>
    /// Indicates the REP state.
    /// </summary>
    public enum REPStateEnumType
    {
        NormalOperation = 0,
        Maintenance = 1,
    }

    #endregion

    public enum ListStatusEnum
    {
        NoLoaded,
        LoadedAndActivate,
        LoadedAndInactive
    }

    /// <summary>
    /// Type of the expired time
    /// </summary>
    public enum ExpiredTimeTypeEnum : int
    {
        Active = 0,
        Expired = 1
    }

    /// <summary>
    /// Specifies the types of funnel
    /// </summary>
    public enum FunnelType
    {
        Funnel1 = 1,
        Funnel2 = 2,
        Both = 3
    }

    /// <summary>
    /// Indicates the modes for the PersonSearch control
    /// </summary>
    public enum PersonSearchModeEnum
    {
        /// <summary>
        /// Indicates that the PersonSearch control was opened by the registration field lookup
        /// </summary>
        Registration = 1,
        /// <summary>
        /// Indicates that the PersonSearch control was opened by the name field lookup
        /// </summary>
        Name = 2
    }

    /// <summary>
    /// Indicates the modes for the CredentialSearch control
    /// </summary>
    public enum CredentialSearchModeEnum
    {
        /// <summary>
        /// Indicates that the CredentialSearch must load all registered credentials from database
        /// </summary>
        All = 1,
        /// <summary>
        /// Indicates that the CredentialSearch must load only the credentials that authenticates in credentials from database
        /// </summary>
        AuthenticatesInCredential = 2
    }


    /// <summary>
    /// Indicates the date type used to DateFilterShortcut control.
    /// </summary>
    public enum DateTypeEnum
    {
        /// <summary>
        /// When the selected value is 'Yesterday'.
        /// </summary>
        Yesterday = 1,
        /// <summary>
        /// When the selected value is 'Today'.
        /// </summary>
        Today = 2,
        /// <summary>
        /// When the selected value is 'current Week'.
        /// </summary>
        Week = 3,
        /// <summary>
        /// When the selected value is 'last Week'.
        /// </summary>
        LastWeek = 4,
        /// <summary>
        /// When the selected value is 'current fifteen days'.
        /// </summary>
        CurrentFifteenDays = 5,
        /// <summary>
        /// When the selected value is 'last fifteen days'.
        /// </summary>
        LastFifteenDays = 6,
        /// <summary>
        /// When the selected value is 'current month'.
        /// </summary>
        Month = 7,
        /// <summary>
        /// When the selected value is 'last month'.
        /// </summary>
        LastMonth = 8,
        /// <summary>
        /// When the selected value is 'To inform'.
        /// </summary>
        ToInform = 9
    }

    /// <summary>
    /// The type of the SystemLog event
    /// </summary>
    public enum SystemLogType : int
    {
        /// <summary>
        /// Specific for internal use. It's used to treat the MRP state events.
        /// </summary>
        EquipmentMRPStateUpdated = -2,
        /// <summary>
        /// Specific for internal use. It's used to treat the bobbin state events.
        /// </summary>
        EquipmentBobbinStateUpdated = -1,
        /// <summary>
        /// Whether the counters of the accesses done by quantity were reset
        /// </summary>
        ResetAllCountersByQuantity = 2,
        /// <summary>
        /// Whether the current area for an entity is reset.
        /// </summary>
        ResetCurrentArea = 3,
        /// <summary>
        /// Whether the counters of the accesses done by shift were reset
        /// </summary>
        ResetAllCountersByShift = 4,
        /// <summary>
        /// Communication service started
        /// </summary>
        CommunicationServiceStarted = 7,
        /// <summary>
        /// Whether the equipment returned to communicate.
        /// </summary>
        EquipmentReturnedToCommunicate = 8,
        /// <summary>
        /// Whether the equipment lost communication.
        /// </summary>
        EquipmentLostCommunication = 9,
        /// <summary>
        /// Whether the areas restart to be controlled.
        /// </summary>
        AreasWithControl = 10,
        /// <summary>
        /// Whether the areas lost control.
        /// </summary>
        AreasWithoutControl = 11,
        /// <summary>
        /// Whether the area was updated to zero.
        /// </summary>
        AreaUpdatedToUndefined = 12,
        /// <summary>
        /// Whether the occupation was reseted.
        /// </summary>
        ResetOccupation = 13,
        /// <summary>
        /// Whether the areas occupation was reseted.
        /// </summary>
        ResetAreasOccupation = 14,
        /// <summary>
        /// Whether the collect record event raises an error.
        /// </summary>
        ErrorAtCollectRecord = 18,
        /// <summary>
        /// Whether the communication service stopped
        /// </summary>
        CommunicationServiceStopped = 19,
        /// <summary>
        /// Whether the equipment is in maintanance.
        /// </summary>
        EquipmentIsInMaintenance = 128,
        /// <summary>
        /// Whether the employer data aren't registered in the equipment.
        /// </summary>
        EquipmentWithoutEmployerData = 131,
        /// <summary>
        /// Whether the equipment was not activated.
        /// </summary>
        EquipmentWithoutInitialActivation = 132,
        /// <summary>
        /// Whether the equipment bobbin state is little paper.
        /// </summary>
        EquipmentBobbinStateIsLittlePaper = 133,
        /// <summary>
        /// Whether the equipment bobbin state is no paper.
        /// </summary>
        EquipmentBobbinStateIsNoPaper = 134,
        /// <summary>
        /// Whether the equipment bobbin state is OK.
        /// </summary>
        EquipmentBobbinStateIsOk = 135,
        /// <summary>
        /// Whether an inquire access is done using an access button
        /// </summary>
        AccessButtonActivatedInEquipment = 150,

        LicenseValid = 151,

        LicenseInvalid = 152,

        AuthorizationExceptionalOnEquipmentTimeout = 153,

        ResetCountersByQuantity = 154,

        ResetCurrentAreaByQuantity = 155,

        ResetCountersByShift = 156,

        ResetCurrentAreaByShift = 157
    }

    /// <summary>
    /// The type of the CCTV's manufacturer
    /// </summary>
    public enum CCTVManufacturerTypeEnum : int
    {
        /// <summary>
        /// Indicates that the CCTV's manufacturer is the Intelbras
        /// </summary>
        Intelbras = 0,
        /// <summary>
        /// Indicates that the CCTV's manufacturer is the Digifort
        /// </summary>
        Digifort = 1
    }

    /// <summary>
    /// The type of the CCTV's connection mode
    /// </summary>
    public enum CCTVConnectionModeTypeEnum : int
    {
        /// <summary>
        /// Indicates that the CCTV's connection mode is internal
        /// </summary>
        InternalConnection = 1,
        /// <summary>
        /// Indicates that the CCTV's connection mode is external
        /// </summary>
        ExternalConnection = 2
    }

    /// <summary>
    /// The possible events raised by the access validation (in full)
    /// </summary>
    [DataContract]
    public enum PasswatchTypeEnum : int
    {
        /// <summary>
        /// Indicates that the access is granted
        /// </summary>
        [EnumMember]
        AccessGranted = 1,
        /// <summary>
        /// Indicates that the access is NOT granted
        /// </summary>
        [EnumMember]
        AccessNotGranted = 2,
        /// <summary>
        /// Indicates that the access is granted but with restrictions
        /// </summary>
        [EnumMember]
        AccessGrantedWithRestrictions = 3
    }

    /// <summary>
    /// The types of the license's status
    /// </summary>
    public enum LicenseStatusTypeEnum : int
    {
        /// <summary>
        /// Indicates that the license status has an error 
        /// </summary>
        OK = 0,
        /// <summary>
        /// Indicates that the license status is OK
        /// </summary>
        Error = 1
    }

    public enum NotificationTypeEnum
    {
        Error = 1,
        DatabaseConnection = 2,
        AreaControl = 3,
        CommunicationConnection = 4,
        EquipmentMode = 5,
        PowerSupply = 6,
        AFD = 7,
        RIM = 8,
        Activation = 9,
        PaperRoll = 10,
        Present = 11,
        Absent = 12,
        PresentCollectREP = 13,
        QrCodeByEmail = 14,
        Mask = 15,
        Temperature = 16,
        MaskAndTemperature = 17,
    }

    public enum NotificationParameterTypeEnum
    {
        ApplicationVersion = 1,
        CommunicationManagerDescription = 2,
        EquipmentDescription = 3,
        EquipmentDescriptionList = 4,
        NotificationStatus = 5,
        DateTime = 6,
        PersonName = 7,
        AccessDirection = 8,
        AreaDescription = 9,
        QrCode = 10,
        Mask = 11,
        Temperature = 12
    }
    /// <summary>
    /// The types of the control time.
    /// </summary>
    public enum GroupTimeControlTypeEnum
    {
        ControlForTimeExit = 0,
        ControlForTimeEntrance = 1
    }

    /// <summary>
    /// The types of extra field.
    /// </summary>
    public enum ExtraFieldTypeEnum
    {
        /// <summary>
        /// A field with alphanumeric caracters allowed.
        /// </summary>
        Text = 1,
        /// <summary>
        /// A checkbox type field.
        /// </summary>
        Boolean = 2,
        /// <summary>
        /// A numeric only text field.
        /// </summary>
        Numeric = 3,
        /// <summary>
        /// A text field with a calendar date and time widget.
        /// </summary>
        DateTime = 4,
        /// <summary>
        /// A text field that accepts only valid times (00:00) - (23:59).
        /// </summary>
        Time = 5,
        /// <summary>
        /// A text field with a calendar date widget.
        /// </summary>
        Date = 6,
        /// <summary>
        /// A e-mail field
        /// </summary>
        Email = 7
    }

    /// <summary>
    /// The number of the extra field
    /// </summary>
    public enum FieldSendQrCodeEmailTypeEnum
    {
        /// <summary>
        /// A extra field 1
        /// </summary>
        Field1 = 1,
        /// <summary>
        /// A extra field 2
        /// </summary>
        Field2 = 2,
        /// <summary>
        /// A extra field 3
        /// </summary>
        Field3 = 3,
        /// <summary>
        /// A extra field 4
        /// </summary>
        Field4 = 4
    }

    /// <summary>
    /// Indicates the type of the authentication performed by the equipment
    /// </summary>
    public enum EquipmentAuthenticationEnumBio : int
    {
        OneToOne = 0,
        OneToMany = 1,
        OneToR = 2
    }

    /// <summary>
    /// Rule type for granted access for a period situation
    /// </summary>
    [DataContract]
    public enum MultiSituationGrantRuleTypeTypeEnum : int
    {
        SatisfyAll = 0,
        SatisfyOneOrMore = 1
    }

    /// <summary>
    /// Indicates the type of the model print
    /// </summary>
    public enum PrintModelTypeEnum : int
    {
        Deskjet = 0,
        Pimaco = 1,
        Both = 2,
        Zebra = 3,
        PimacoSLP650 = 4,
    }

    /// <summary>
    /// Indicates the type of the mode print
    /// </summary>
    public enum PrintModeTypeEnum : int
    {
        PrintAutomatically = 0,
        NotPrintAutomatically = 1,
        AskBeforePrint = 2
    }

    /// <summary>
    /// Indicates the type of the language
    /// </summary>
    public enum LanguageTypeEnum : int
    {
        Portuguese = 0,
        English = 1
    }

    /// <summary>
    /// Indicates the type of configuration of the photo of the person on the equipament display
    /// </summary>
    public enum SettingPhotoTypeEnum : int
    {
        NoPhoto = 0,
        Registered = 1,
        Realtime = 2
    }

    /// <summary>
    /// Indicates the type of verification mode
    /// </summary>
    public enum VerificationModeTypeEnum : int
    {
        FaceOrCardOrPassword = 0,
        OnlyFace = 1,
        FaceAndCard = 2,
        FaceAndPassword = 3,
        FaceAndCardAndPassword = 4
    }

    /// <summary>
    /// Indicates the type of verification mode
    /// </summary>
    public enum VerificationDMPCMModeTypeEnum : int
    {
        Fingerprint = 0,
        Card = 1,
        CardOrFingerprint = 2,
        CardAndFingerprint = 3,
        CardAndPassword = 4
    }

    /// <summary>
    /// Indicates the type of door sensor
    /// </summary>
    public enum DoorSensorTypeEnum : int
    {
        None = 0,
        NormallyOpen = 1,
        NormallyClosed = 2
    }

    /// <summary>
    /// Indicates the type of output signal
    /// </summary>
    public enum OutputSignalTypeEnum : int
    {
        Wiegand26 = 0,
        Wiegand34 = 1
    }

    /// <summary>
    /// Indicates the type of the language for device mode facial.
    /// </summary>
    public enum LanguageFacialType
    {
        English = 0,
        Portuguese = 1,
    }

    /// <summary>
    /// Indicates the type of the sensor for device mode facial.
    /// </summary>
    public enum SensorFacialType
    {
        None = 0,
        NormallyOpen = 1,
        NormallyClosed = 2
    }

    /// <summary>
    /// Indicates the type of the card read output for device mode facial.
    /// </summary>
    public enum CardReadFacialType
    {
        Wiegand26 = 0,
        Wiegand34 = 1
    }

    /// <summary>
    /// Indicates the type of the Verification Mode for device mode facial.
    /// </summary>
    public enum VerificationModeFacialType
    {
        FaceOUCardOUPin = 9,
        Face = 10,
        FaceECard = 11,
        FaceEPin = 12,
        FaceECardEPin = 13,
    }

    /// <summary>
    /// Indicates the type of the capture photo mode for device mode facial.
    /// </summary>
    public enum CapturePhotoModeFacialType
    {
        None = 0,
        UserPhoto = 1,
        RealTimeCamera = 2,
    }

    /// <summary>
    /// Represents the type of communication manager.
    /// </summary>
    public enum CommunicationManagerTypeEnum : int
    {
        Client = 0,
        Server = 1,
        ExportAndImport = 2
    }

    /// <summary>
    /// Indicates the customizations types. 
    /// </summary>
    public enum CustomizationTypeEnum : int
    {
        CT_10159 = 1,
        CT_10330 = 2,
        CT_10386 = 3,
        CT_10426 = 4,
        CT_10771 = 5,
        CT_10794 = 6,
        CT_10833 = 7,
        CT_10834 = 8,
        CT_10881 = 9,
        CT_10999 = 10
    }

    /// <summary>
    /// Indicates the card writer status types.
    /// </summary>
    public enum CardWriterStatusEnum : int
    {
        Success = 0,
        CardWriterNotFound = 1,
        CardNotFound = 2,
        LoginFailed = 3,
        WritingFailed = 4,
        ReadingFailed = 5
    }

    /// <summary>
    /// Indicates the Key types for card writer. 
    /// </summary>
    public enum KeyTypeEnum : int
    {
        KeyTypeA = 0,
        KeyTypeB = 1
    }

    /// <summary>
    /// Indicates the load biometric types.
    /// </summary>
    public enum LoadBiometricTypeEnum : int
    {
        All = 0,
        Sagem = 1,
        Inttelix = 2,
        None = 3,
        Suprema = 4,
        InttelixSagem = 5,
        InttelixSuprema = 6,
        SagemSuprema = 7
    }

    /// <summary>
    /// Indicates the field which is inquired to retrieve the fingerprint.
    /// </summary>
    public enum PersonFieldQueryFingerprintTypeEnum : int
    {
        RegistrationNumber = 0,
        OrganizationalStructure = 1,
        ExtraField1 = 2,
        ExtraField2 = 3,
        ExtraField3 = 4,
        ExtraField4 = 5,
        ExtraField5 = 6,
        ExtraField6 = 7,
        ExtraField7 = 8,
        ExtraField8 = 9,
        ExtraField9 = 10,
        ExtraField10 = 11,
        DocumentField1 = 12,
        DocumentField2 = 13,
        DocumentField3 = 14
    }

    /// <summary>
    /// Indicates the validation type to the documents fields.
    /// </summary>
    public enum DocumentValidationTypeEnum : int
    {
        None = 0,
        CPF = 1,
        PIS = 2
    }

    /// <summary>
    /// Indicates the condition deactivation types.
    /// </summary>
    public enum ConditionDeactivationTypeEnum : int
    {
        EntranceAndExit = 0,
        Exit = 1
    }

    /// <summary>
    /// Indicates the condition Sensor types.
    /// </summary>
    public enum EEnabledSensorType
    {
        NotConfigured = 0,
        Enabled = 1,
        Disabled = 2,
    }

    /// <summary>
    /// Indicates the condition Firmware types.
    /// </summary>
    public enum EFirmwareType
    {
        REP = 0,
        MiniREPSuprema = 1,
        MiniREPSagem = 2,
        REPHome = 3,
        REPIII = 4,
    }

    /// <summary>
    /// /// Indicates the condition Biometric Sensor types.
    /// </summary>
    public enum EBiometricSensorType
    {
        Sagem = 0,
        Suprema = 1,
        Fujitsu = 2,
        ILock = 3,
        Virdi = 4,
        Undefined = 254,
        None = 255,
    }

    /// <summary>
    /// Indicates the condition Violation Sensor types.
    /// </summary>
    public enum ViolationSensorType
    {
        Closed = 0,
        Open = 1
    }

    /// <summary>
    /// Indicates the template biometric format types.
    /// </summary>
    public enum TemplateBiometricFormatTypeEnum
    {
        SagemPkComp = 0,
        Suprema = 1,
        Fujitsu = 2,
        Ilock = 3,
        VirdiUnion = 4,
        Ansi378 = 5,
        IsoFmr = 6,
        IsoFmcNs = 7,
        IsoFmcCs = 8,
        IsoFmcCsAA = 9,
        MinexA = 10,
        DinV66400 = 11,
        DinV66400AA = 12,
        SecuKey = 13
    }

    /// <summary>
    /// Indicates the quantity of template biometric for new format.
    /// </summary>
    public enum QuantityTemplateBiometricTypeEnum
    {
        Indefined = 0,
        OneFinger = 1,
        TwoFinger = 2
    }

    /// <summary>
    /// Indicates the list of expressionsBoolean types for exclude 
    /// </summary>
    public enum ListOfExpressionsBooleanExcludeTypeEnum
    {
        All = 0,
        ExpressionsBoolean = 1,
        TimesDaily = 2,
        Periods = 3,
        Holidays = 4,
        Actions = 5
    }

    /// <summary>
    /// Indicates the output type 
    /// </summary>
    public enum OutputTypeEnum : int
    {
        Output1 = 0,
        Output2 = 1,
        Output3 = 2,
        Output4 = 3,
    }

    public enum InputTypeEnum : int
    {
        Input1 = 0,
        Input2 = 1,
        Input3 = 2,
        Input4 = 3,
        Input5 = 4,
        Input6 = 5,
        Input7 = 6,
        Input8 = 7
    }

    public enum TermTypeEnum
    {
        Input = 0,
        Output = 100,
        Expression = 200
    }

    public enum BehaviorOutputTypeEnum
    {
        Pulse = 0,
        Level = 1
    }

    public enum PulseTypeEnum
    {
        Up = 0,
        Down = 1
    }

    public enum TransitionTypeEnum
    {
        Up = 0,
        Down = 1
    }

    public enum PulseDurationResetTypeEnum
    {
        NotStart = 0,
        Start = 1
    }

    public enum StatusOutputTypeEnum
    {
        Disabled = 0,
        Activated = 1,
        NotControlledBySoftware = 2
    }

    public enum PeriodTableTypeEnum : int
    {
        Weekly = 0,
        Monthly = 1,
        Other = 2
    }

    public enum ExpressionsBooleanTypeEnum : int
    {
        AND = 0,
        OR = 1,
        NOT = 2,
        NAND = 3,
        NOR = 4,
        XOR = 5
    }

    public enum OccurrenceTypeEnum : int
    {
        Undefined = 0,       // Indefinido 
        Intrusion = 1,       // Intrusão - "em porta"
        EntranceIntrusion = 2,       // Intrusão de Entrada - "em catraca"
        ExitIntrusion = 3,       // Intrusão de Saída - "em catraca"
        DoorOpened = 4,       // Porta aberta
        DoorClosed = 5,       // Porta fechada
        AccessButton = 6,       // Acesso por botoeira
        Actived = 7,       // ativado para CIO 
        Disabled = 8        // desativado para CIO   
    }

    public enum PresentNotifiesTypeEnum : int
    {
        All = 0,
        Email = 1,
        SMS = 2,
        None = 3
    }

    public enum AbsentNotifiesTypeEnum : int
    {
        All = 0,
        Email = 1,
        SMS = 2,
        None = 3
    }

    /// <summary>
    /// Indentify notification type for send data of notificador
    /// </summary>
    public enum NotificationType : int
    {
        Alls = 0,
        Emails = 1,
        SMS = 2,
        None = 3
    }

    /// <summary>
    /// Indentify the types of Sortition
    /// </summary>
    public enum SotitionTypeEnum : int
    {
        DenyAccess = 0,
        AllowAccessWithSoundWarn = 1,
        DenyOnlyThisAccess = 2,
        DenyAccessTimeInterval = 3
    }

    /// <summary>
    /// 
    /// </summary>
    public enum GroupControlSortitionTypeEnum : int
    {
        None = 0,
        Origin = 1,
        Destination = 2,
        Both = 3
    }

    public enum BiometricEntityTypeCapture
    {
        Person = 0,
        Visitor = 1
    }

    /// <summary>
    /// The schedule of exportation type
    /// </summary>
    public enum ScheduleTypeEnum
    {
        AccessLog = 0,
        Rep = 1
    }

    public enum LayoutTypeEnum
    {
        Content,
        Name
    }

    /// <summary>
    /// The control time type
    /// </summary>
    public enum ControlTimeTypeEnum
    {
        Restriction = 0,
        Liberation = 1
    }

    /// <summary>
    /// Process data type enum
    /// </summary>
    public enum ProcessDataTypeEnum
    {
        Separator = 0,
        Positional = 1
    }

    public enum OrganizationalStructureDtoTypeEnum
    {
        /// <summary>
        /// When the structure is an Enterprise
        /// </summary>
        Company = 0,
        /// <summary>
        /// When the structure is a Department
        /// </summary>
        Department = 1
    }

    /// <summary>
    /// TimeSlot role restriction precedence type enum
    /// </summary>
    public enum TimeSlotRoleRestrictionPrecedenceTypeEnum
    {
        /// <summary>
        /// The none
        /// </summary>
        None = 0,
        /// <summary>
        /// The there can be no passage
        /// </summary>
        ThereCanBeNoPassage = 1,
        /// <summary>
        /// The there must be passage
        /// </summary>
        ThereMustBePassage = 2
    }

    /// <summary>
    /// TimeSlot type restriction precedence type enum
    /// </summary>
    public enum TimeSlotTypeRestrictionPrecedenceTypeEnum
    {
        /// <summary>
        /// The access
        /// </summary>
        Access = 0,
        /// <summary>
        /// The register
        /// </summary>
        Register = 1,
        /// <summary>
        /// The access and register
        /// </summary>
        AccessAndRegister = 2,
        /// <summary>
        /// The Mask
        /// </summary>
        Mask = 3,
        /// <summary>
        /// The temperature
        /// </summary>
        Temperature = 4,
        /// <summary>
        /// The mask and temperature
        /// </summary>
        MaskAndTemperature = 5
    }

    /// <summary>
    /// Precedence UseMask and Temperature type enum.
    /// </summary>
    public enum PrecedenceUseMaskAndTemperatureTypeEnum
    {
        OK = 0,
        NotUsedMask = 1,
        TemperatureOffRange = 2,
        NotUsedMaskAndTemperatureOffRange = 3
    }

    /// <summary>
    /// 
    /// </summary>
    public enum RestrictionForAuthorizerTypeEnum
    {
        None = 0,
        DefaultOnly = 1,
        AdvencedOnly = 2,
        DefaultAndAdvenced = 3
    }
    /// <summary>
    /// Hide some enum for swagger documentation
    /// </summary>
    public class SwaggerHiddenEnum : Attribute { }

    /// <summary>
    /// Indicates the type of the access profile
    /// </summary>
    public enum AccessProfileDtoTypeEnum : int
    {
        ForPersons = 0,
        ForVisitors = 1,
        ForCredentials = 2
    }

    /// <summary>
    /// Indicates the type of the logs.
    /// </summary>
    public enum LogTypeEnum : int
    {
        Access = 0,
        Register = 1,
        AccessAndRegister = 2,
        Mask = 3,
        Temperature = 4,
        MaskAndTemperature = 5
    }

    /// <summary>
    /// Indicates the type of the vehicle plate validate.
    /// </summary>
    public enum VehiclePlateValidateTypeEnum : int
    {
        /// <summary>
        /// The none
        /// </summary>
        None = 1,
        /// <summary>
        /// The brazil
        /// </summary>
        Brazil = 2
    }

    /// <summary>
    /// 
    /// </summary>
    public enum PhotoTypeEnum : int
    {
        /// <summary>
        /// The face
        /// </summary>
        Face = 0,
        /// <summary>
        /// The document front
        /// </summary>
        DocFront = 1,
        /// <summary>
        /// The document verse
        /// </summary>
        DocVerse = 2
    }

    /// <summary>
    /// Indicates the type of the communication response data for dto.
    /// </summary>
    [DataContract]
    public enum CommunicationResponseTypeEnum : int
    {
        /// <summary>
        /// Response for inquire access normal.
        /// </summary>
        [DataMember]
        Normal = 1,
        /// <summary>
        /// Response for inquire access for authorizer.
        /// </summary>
        [DataMember]
        ForAuthorizer = 2
    }

    /// <summary>
    /// Indicates the type of the send qr code
    /// </summary>
    public enum SendQrCodeTypeEnum : int
    {
        /// <summary>
        /// Sends using MD/DMP Notificador
        /// </summary>
        Notifier = 1,
        /// <summary>
        /// Sends using whatsapp
        /// </summary>
        WhatsApp = 2,
        /// <summary>
        /// Using send grid
        /// </summary>
        SendGrid = 3
    }

    /// <summary>
    /// Indicates the type of Anti-Passback
    /// </summary>
    public enum AntiPassbackTypeEnum : int
    {
        AntiPassback0 = 0,
        AntiPassback1 = 1,
        AntiPassback2 = 2,
        AntiPassback3 = 3,
        AntiPassback4 = 4,
        AntiPassback5 = 5,
        AntiPassback6 = 6,
        AntiPassback7 = 7,
        AntiPassback8 = 8,
        AntiPassback9 = 9,
        AntiPassback10 = 10,
        AntiPassback11 = 11,
        AntiPassback12 = 12,
        AntiPassback13 = 13,
        AntiPassback14 = 14,
        AntiPassback15 = 15,
        AntiPassback16 = 16,
        AntiPassback17 = 17,
        AntiPassback18 = 18,
        AntiPassback19 = 19,
        AntiPassback20 = 20,
        AntiPassback21 = 21
    }

    /// <summary>
    /// Indicates the type of interlock
    /// </summary>
    public enum InterLockTypeEnum : int
    {
        InterLock0 = 0,
        InterLock1 = 1,
        InterLock2 = 2,
        InterLock3 = 3,
        InterLock4 = 4,
        InterLock5 = 5
    }

    /// <summary>
    /// Indicates the type of Reader Type
    /// </summary>
    public enum InBioToWayTypeEnum : int
    {
        Unidirecional = 0,
        Bidirecional = 1
    }

    /// <summary>
    /// Indicates the type  of Rule 
    /// </summary>
    public enum RuleTypeEnum : int
    {
        Default = 1,
        Advanced = 2
    }
}
