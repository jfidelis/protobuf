﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppSerializerClass
{
    /// <summary>
    /// Provides methods for binary serialization (protocol buffers)
    /// </summary>
    public static class SerializationHelper
    {
        #region Fields
        private static bool _isInitialized = false;
        private static readonly object _syncRoot = new object();
        #endregion

        #region SerializationHelper
        /// <summary>
        /// Initializes the SerializationHelper class
        /// </summary>
        static SerializationHelper()
        {
            if (!_isInitialized)
            {
                lock (_syncRoot)
                {
                    if (!_isInitialized)
                    {
                        Initialize();

                        _isInitialized = true;
                    }
                }
            }
        }
        #endregion

        #region Initialize
        /// <summary>
        /// Initializes the types for serialization
        /// </summary>
        private static void Initialize()
        {
            //RuntimeTypeModel.Default.Add(typeof(CommandResult), true);
            //RuntimeTypeModel.Default.Add(typeof(EquipmentStatus), true);
            //RuntimeTypeModel.Default.Add(typeof(RepositioningPointerParams), true);

            //RuntimeTypeModel.Default.CompileInPlace();
            Serializer.GlobalOptions.InferTagFromName = true;
        }
        #endregion

        #region Serialize
        /// <summary>
        /// Serializes an object into a byte array.
        /// </summary>
        /// <typeparam name="T">The type of the object</typeparam>
        /// <param name="input">The object</param>
        /// <returns>A byte array</returns>
        public static byte[] Serialize<T>(T input)
        {
            byte[] output = null;

            using (MemoryStream stream = new MemoryStream())
            {
                Serializer.Serialize(stream, input);

                output = stream.ToArray();
            }

            return output;
        }
        #endregion

        #region Deserialize
        /// <summary>
        /// Deserializes a byte array into an object
        /// </summary>
        /// <typeparam name="T">The type of the object</typeparam>
        /// <param name="input">The byte array</param>
        /// <returns>An object</returns>
        public static T Deserialize<T>(byte[] input)
        {
            T output = default(T);

            using (MemoryStream stream = new MemoryStream(input))
            {
                output = Serializer.Deserialize<T>(stream);
            }

            return output;
        }
        #endregion

        #region DeserializeTryParse        
        /// <summary>
        /// Deserializes the try parse.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <param name="parametersDict">The parameters dictionary.</param>
        /// <returns></returns>
        public static bool DeserializeTryParse(byte[] input, out Dictionary<decimal, bool> parametersDict)
        {
            try
            {
                using (MemoryStream stream = new MemoryStream(input))
                {
                    parametersDict = Serializer.Deserialize<Dictionary<decimal, bool>>(stream);
                }
                return true;
            }
            catch (System.Exception)
            {
                parametersDict = null;
                return false;
            }
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static string GetProto<T>()
        {
            return Serializer.GetProto<T>();
        }
    }
}
