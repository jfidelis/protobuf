﻿using System;
using System.Runtime.Serialization;

namespace ConsoleAppSerializerClass
{
    /// <summary>
    /// Represents the Allow an Access parameters
    /// </summary>
    [DataContract]
    [Serializable]
    public class AllowAnAccessParams
    {
        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        /// <value>
        /// The code.
        /// </value>
        [DataMember(Order = 1)]
        public string Code { get; set; }
        /// <summary>
        /// Gets or sets the justification.
        /// </summary>
        /// <value>
        /// The justification.
        /// </value>
        [DataMember(Order = 2)]
        public string Justification { get; set; }

        /// <summary>
        /// Gets or sets the access direction type enum.
        /// </summary>
        /// <value>
        /// The access direction type enum.
        /// </value>
        [DataMember(Order = 3)]
        public AccessDirectionType AccessDirectionTypeEnum { get; set; }
    }
}
